﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
   public class Foto
    {
        private int id;
        private string ruta;
        private int idDepartamento;

        public Foto() { }

        public Foto(int id, string ruta, int idDepartamento)
        {
            this.id = id;
            this.ruta = ruta;
            this.idDepartamento = idDepartamento;
        }

        public int Id
        {

            get { return id; }
            set { id = value; }

        }


        public string Ruta
        {

            get { return ruta; }
            set { ruta = value; }

        }


        public int IDdepartamento
        {

            get { return idDepartamento; }
            set { idDepartamento = value; }

        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
  public class Departamento
    {
        private int id;
        private string direccion;
        private string caracteristicas;
        private int valorNoche;
        private int dormitorios;
        private int camas;
        private ComunaRequest  comuna;
        private List<Foto> fotos;
        private List<Equipamiento> equipamientos;

        public Departamento() {}

        public Departamento(int id, string direccion, string caracteristicas, int valorNoche, int dormitorios, int camas, ComunaRequest comuna, List<Foto> fotos, List<Equipamiento> equipamientos)
        {
            this.id = id;
            this.direccion = direccion;
            this.caracteristicas = caracteristicas;
            this.valorNoche = valorNoche;
            this.dormitorios = dormitorios;
            this.camas = camas;
            this.comuna = comuna;
            this.fotos = fotos;
            this.equipamientos = equipamientos;
        }


        public int Id
        {

            get { return id; }
            set { id = value; }
        }

        public string Direccion
        {

            get { return direccion; }
            set { direccion = value; }
        }

        public string Caracteristicas
        {

            get { return caracteristicas; }
            set { caracteristicas = value; }
        }

        public int ValorNoche
        {

            get { return valorNoche; }
            set { valorNoche = value; }
        }


        public int Dormitorios
        {

            get { return dormitorios; }
            set { dormitorios = value; }
        }

        public int Camas
        {

            get { return camas; }
            set { camas = value; }
        }

        public ComunaRequest Comuna
        {

            get { return comuna; }
            set { comuna = value; }
        }

        public List<Foto> Fotos
        {

            get { return fotos; }
            set { fotos = value; }
        }

        public List<Equipamiento> Equipamientos
        {

            get { return equipamientos; }
            set { equipamientos = value; }
        }
    }
}

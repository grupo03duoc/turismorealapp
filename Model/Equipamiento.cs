﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
   public class Equipamiento
    {
        private int id;
        private string nombre;
        private int inventario;
        private int valorUnitario;

        public Equipamiento() { }

        public Equipamiento(int id, string nombre, int inventario, int valorUnitario)
        {
            this.id = id;
            this.nombre = nombre;
            this.inventario = inventario;
            this.valorUnitario = valorUnitario;
        }

        public int Id
        {

            get { return id; }
            set { id = value; }
        }

        public string Nombre
        {

            get { return nombre; }
            set { nombre = value; }
        }
        public int Inventario
        {

            get { return inventario; }
            set { inventario = value; }
        }

        public int ValorUnitario
        {

            get { return valorUnitario; }
            set { valorUnitario = value; }
        }

    }
}


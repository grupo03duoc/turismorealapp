﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Chofer
    {
            public int id { get; set; }
            public string nombre { get; set; }
            public string rut { get; set; }
            public string vehiculo { get; set; }
            public Empresa empresa { get; set; }

            public Chofer() { }

            public Chofer(int id, string nombre, string rut, string vehiculo, Empresa empresa)
            {
                this.id = id;
                this.nombre = nombre;
                this.rut = rut;
                this.vehiculo = vehiculo;
                this.empresa = empresa;
            }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Provincia
    {
        private int id;
        private string nombre;
        private int region;

        public Provincia() { }

        public Provincia(int id, string nombre, int region)
        {
            this.id = id;
            this.nombre = nombre;
            this.region = region;
        }

        public int Id
        {

            get { return id; }
            set { id = value; }
        }

        public string Nombre
        {

            get { return nombre; }
            set { nombre = value; }
        }
        public int Region
        {

            get { return region; }
            set {region = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Tour
    {
        private int id;
        private string nombre;
        private string descripcion;
        private int valor_persona;
        private string region;

        public Tour() { }

        public Tour(int id, string nombre, string descripcion, int valor_persona, string region)
        {
            this.id = id;
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.valor_persona = valor_persona;
            this.region = region;
        }

        public int Id
        {

            get { return id; }
            set { id = value; }
        }

        public string Nombre
        {

            get { return nombre; }
            set { nombre = value; }
        }
        public string Descripcion
        {

            get { return descripcion; }
            set { descripcion = value; }
        }

        public int Valor_Persona
        {

            get { return valor_persona; }
            set { valor_persona = value; }
        }

        public string Region
        {

            get { return region; }
            set { region = value; }
        }


    }
}

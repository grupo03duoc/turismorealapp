﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Usuario
    {
        private int id;
        private string nombre;
        private string username;
        private string correo;
        private List<Perfil> perfil;
        private bool estado;
        private string password;

        public Usuario() { }

        public Usuario(int id, string nombre, string username, string correo, List<Perfil> perfil, bool estado)
        {
            this.id=id;
            this.nombre = nombre;
            this.username = username;
            this.correo = correo;
            this.perfil = perfil;
            this.estado = estado;
        }

        public int Id
        {

            get { return id; }
            set { id = value; }
        }

        public String Nombre
        {

            get { return nombre; }
            set { nombre = value; }
        }


        public String Correo
        {

            get { return correo; }
            set { correo = value; }
        }


        public List<Perfil> Perfil
        {

            get { return perfil; }
            set { perfil = value; }
        }


        public Boolean estadoUsuario
        {

            get { return estado; }
            set { estado = value; }
        }

        public String Username
        {

            get { return username; }
            set { username = value; }
        }


        public String Password
        {

            set { password = value; }
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
   public class Region
    {
        private int id;
        private string nombre;
        private string abreviatura;
        private string capital;

        public Region() { }

        public Region(int id, string nombre, string abreviatura, string capital)
        {
            this.id = id;
            this.nombre = nombre;
            this.abreviatura = abreviatura;
            this.capital = capital;
        }

        public int Id
        {

            get { return id; }
            set { id = value; }
        }


        public string Nombre
        {

            get { return nombre; }
            set { nombre = value; }
        }

        public string Abreviatura
        {

            get { return abreviatura; }
            set { abreviatura = value; }
        }


        public string Capital
        {

            get { return capital; }
            set { capital = value; }
        }

    }
}

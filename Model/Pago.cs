﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Pago
    {

        public int id { get; set; }
        public string fecha { get; set; }
        public int monto { get; set; }
        public TipoPago tipoPago { get; set; }
        public int idReserva { get; set; }

        public Pago() { }

        public Pago(int id, string fecha, int monto,TipoPago tipoPago, int idReserva)
        {
            this.id = id;
            this.fecha = fecha;
            this.monto = monto;
            this.tipoPago = tipoPago;
            this.idReserva = idReserva;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class LoginForm
    {

        public string username { get; set; }
        public string password { get; set; }

        public LoginForm() { }

        public LoginForm(string username, string password)
        {
            this.username = username;
            this.password = password;

        }

    }

}

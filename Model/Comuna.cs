﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Comuna
    {

        private int id;
        private string nombre;
        private int provincia;

        public Comuna() { }

        public Comuna(int id, string nombre, int provincia)
        {
            this.id = id;
            this.nombre = nombre;
            this.provincia = provincia;
        }

        public int Id
        {

            get { return id; }
            set { id = value; }
        }

        public string Nombre
        {

            get { return nombre; }
            set { nombre = value; }
        }
        public int Provincia
        {

            get { return provincia; }
            set { provincia = value; }
        }
    }

}

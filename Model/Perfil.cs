﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Perfil
    {

        private int id;
        private string nombre;

        public Perfil() { }

        public Perfil(int id, string nombre)
        {
            this.id = id;
            this.nombre = nombre;
     
        }

        public int Id
        {

            get { return id; }
            set { id = value; }

        }


        public string Nombre
        {

            get { return nombre; }
            set { nombre = value; }

        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Empresa
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string rut { get; set; }
        public string rubro { get; set; }

        public Empresa() { }

        public Empresa(int id, string nombre, string rut, string rubro)
        {
            this.id = id;
            this.nombre = nombre;
            this.rut = rut;
            this.rubro = rubro;
        }
    }
}

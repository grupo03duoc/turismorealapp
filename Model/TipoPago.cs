﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
   public class TipoPago
    {

        public int id { get; set; }
        public string descripcion { get; set; }
       

        public TipoPago() { }

        public TipoPago(int id, string descripcion)
        {
            this.id = id;
            this.descripcion = descripcion;
        }

    }

}

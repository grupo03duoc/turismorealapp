﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class Reserva
    {
        private int id;
        private string fechaInicio;
        private string fechaTermino;
        private int personas;
        private int total;
        private int usuario;
        private int departamento;


        public Reserva() { }

        public Reserva(int id, string fechaInicio, string fechaTermino, int personas, int total, int usuario, int departamento)
        {
            this.id = id;
            this.fechaInicio = fechaInicio;
            this.fechaTermino = fechaTermino;
            this.personas = personas;
            this.total = total;
            this.usuario = usuario;
            this.departamento = departamento;

        }

        public int Id
        {

            get { return id; }
            set { id = value; }
        }

        public String FechaInicio
        {

            get { return fechaInicio; }
            set { fechaInicio = value; }
        }


        public String FechaTermino
        {

            get { return fechaTermino; }
            set { fechaTermino = value; }
        }

        public int Personas
        {

            get { return personas; }
            set { personas = value; }
        }

        public int Total
        {

            get { return total; }
            set { total = value; }
        }

        public int Usuario
        {

            get { return usuario; }
            set { usuario = value; }
        }

        public int Departamento
        {

            get { return departamento; }
            set { departamento = value; }
        }



    }

}


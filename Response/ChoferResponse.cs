﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Response
{
   public class ChoferResponse
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string rut { get; set; }
        public string vehiculo { get; set; }
        public int empresa { get; set; }

        public ChoferResponse() { }

        public ChoferResponse(int id, string nombre, string rut, string vehiculo, int empresa)
        {
            this.id = id;
            this.nombre = nombre;
            this.rut = rut;
            this.vehiculo = vehiculo;
            this.empresa = empresa;
        }

    }
}

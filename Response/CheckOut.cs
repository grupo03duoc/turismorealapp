﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Response
{
   public class CheckOut
    {

        public int id { get; set; }
        public string nombreEmpleado { get; set; }
        public int multa { get; set; }
        public int idReserva { get; set; }

        public CheckOut() { }

        public CheckOut(int id, string nombreEmpleado, int multa, int idReserva)
        {
            this.id = id;
            this.nombreEmpleado = nombreEmpleado;
            this.multa = multa;
            this.idReserva = idReserva;

        }

    }
}

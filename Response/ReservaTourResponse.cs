﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Response
{
    public class ReservaTourResponse
    {
        public int reserva { get; set; }
        public int tour { get; set; }

        public ReservaTourResponse() { }

        public ReservaTourResponse(int reserva, int tour)
        {
            this.reserva = reserva;
            this.tour = tour;
          
        }
    }
}

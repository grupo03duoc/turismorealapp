﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Response
{
   public class TourResponse
    {

        public int id { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public int valor_persona { get; set; }
        public int region { get; set; }

        public TourResponse() { }

        public TourResponse(int id, string nombre, string descripcion, int valor_persona, int region)
        {
            this.id = id;
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.valor_persona = valor_persona;
            this.region = region;
        }
    }
}

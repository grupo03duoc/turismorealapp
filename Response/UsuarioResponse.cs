﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Response
{
    //Clase que se usa  como response a la API, para los metodos Editar, Desactivar y Crear.
    public class UsuarioResponse
    {

        public int id { get; set; }
        public string nombre { get; set; }
        public string username { get; set; }
        public string correo { get; set; }
        public string perfil { get; set; }
        public int habilitado { get; set; }
        public string password { get; set; }



        public UsuarioResponse() { }

        public UsuarioResponse(int id,string nombre, string username, string correo, string perfil, int habilitado, string password)
        {
            this.id = id;
            this.nombre = nombre;
            this.username = username;
            this.correo = correo;
            this.perfil = perfil;
            this.habilitado = habilitado;
            this.password = password;
        }
    }
}


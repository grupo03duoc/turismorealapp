﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Response
{
    public class EquipamientoResponse
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public int inventario { get; set; }
        public int valorUnitario { get; set; }

        public EquipamientoResponse() { }

        public EquipamientoResponse(int id, string nombre, int inventario, int valorUnitario)
        {
            this.id = id;
            this.nombre = nombre;
            this.inventario = inventario;
            this.valorUnitario = valorUnitario;

        }
    }
}

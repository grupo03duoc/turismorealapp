﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Response
{
    public class DepartamentoResponse
    {

        public int id { get; set; }
        public string direccion { get; set; }
        public string caracteristicas { get; set; }
        public int valorNoche { get; set; }
        public int dormitorios { get; set; }
        public int camas { get; set; }
        public int comuna { get; set; }
        public int estado { get; set; }


        public DepartamentoResponse() { }

        public DepartamentoResponse(int id, string direccion, string caracteristicas, int valorNoche, int dormitorios, int camas, int comuna, int estado)
        {
            this.id = id;
            this.direccion = direccion;
            this.caracteristicas = caracteristicas;
            this.valorNoche = valorNoche;
            this.dormitorios = dormitorios;
            this.camas = camas;
            this.comuna = comuna;
            this.estado = estado;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Response
{
    public class CheckIn
    {
        public int id { get; set; }
        public string nombreEmpleado { get; set; }
        public int conforme { get; set; }
        public int idReserva { get; set; }

        public CheckIn() { }

        public CheckIn(int id, string nombreEmpleado, int conforme, int idReserva)
        {
            this.id = id;
            this.nombreEmpleado = nombreEmpleado;
            this.conforme = conforme;
            this.idReserva = idReserva;

        }
    }
}

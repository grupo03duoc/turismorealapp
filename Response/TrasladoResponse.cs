﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Response
{
    public class TrasladoResponse
    {
        public int id { get; set; }
        public string fecha { get; set; }
        public string ubicacion { get; set; }
        public int chofer { get; set; }
        public int reserva { get; set; }


        public TrasladoResponse() { }

        public TrasladoResponse(int id, string fecha, string ubicacion, int chofer, int reserva)
        {
            this.id = id;
            this.fecha = fecha;
            this.ubicacion = ubicacion;
            this.chofer = chofer;
            this.reserva = reserva;

        }
    }
}

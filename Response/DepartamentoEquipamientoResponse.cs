﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Response
{
    public class DepartamentoEquipamientoResponse
    {

        public int id { get; set; }
        public int idEquipamiento { get; set; }

        public DepartamentoEquipamientoResponse() { }

        public DepartamentoEquipamientoResponse(int id, int idEquipamiento)
        {
            this.id = id;
            this.idEquipamiento = idEquipamiento;

        }
      
    }

}


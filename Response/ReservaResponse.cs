﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Response
{
    public class ReservaResponse
    {

        public int id { get; set; }
        public string fechaInicio { get; set; }
        public string fechaTermino { get; set; }
        public int personas { get; set; }
        public int total { get; set; }
        public int usuario { get; set; }
        public int departamento { get; set; }



        public ReservaResponse() { }

        public ReservaResponse(int id, string fechaInicio, string fechaTermino, int personas, int total, int usuario, int departamento)
        {
            this.id = id;
            this.fechaInicio = fechaInicio;
            this.fechaTermino = fechaTermino;
            this.personas = personas;
            this.total = total;
            this.usuario = usuario;
            this.departamento = departamento;
        }
    }
}

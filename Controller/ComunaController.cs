﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Controller
{
    public class ComunaController
    {
        //Metodo que hace un llamado a la API de tipo GET, la cual recibe una id de Provincia y devuelve una lista de comunas, que despues se retorna para su visualizacion en el modulo de Departamento.
        private static string listarComunasURL = "lista/comunas/";
        public static async Task<List<Comuna>> listarComunas(int idProvincia)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarComunasURL + idProvincia);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Comuna> comunas = await response.Content.ReadAsAsync<List<Comuna>>();
                    return comunas;
                }
                else
                {
                    return null;
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Model;
using System.Threading.Tasks;
using Response;

namespace Controller
{
    public class EquipamientoController
    {

        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de Departamentos por Comuna, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarEquipamientoURL = "/equipamiento/all";
        public static async Task<List<Equipamiento>> listarEquipamientos()
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarEquipamientoURL);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Equipamiento> equipamientos = await response.Content.ReadAsAsync<List<Equipamiento>>();

                    return equipamientos;
                }
                else
                {
                    return null;
                }
            }
        }


        //Metodo que hace un llamado a la API de tipo POST, el cual permite crear un departamento para luego enviarlo como JSON a la API.
        private static string InsertarEquipamientoURL = "equipamiento/create";
        public static async Task<bool> insertarEquipamiento(string nombre, int inventario, int valorUnitario)
        {

            EquipamientoResponse equipamiento = new EquipamientoResponse();
            equipamiento.id= 0;
            equipamiento.nombre = nombre;
            equipamiento.inventario = inventario;
            equipamiento.valorUnitario = valorUnitario;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(InsertarEquipamientoURL, equipamiento);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                
                }
                else
                {
                    return false;
                }

            }
        }



        //Metodo que hace un llamado a la API de tipo POST, el cual permite crear un departamento para luego enviarlo como JSON a la API.
        private static string EliminarEquipamientoURL = "equipamiento/delete/";
        public static async Task<bool> eliminarEquipamiento(int idEquipamiento)
        {

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(EliminarEquipamientoURL+ idEquipamiento);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                 
                }
                else
                {
                    return false;
                }
            }
        }


        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de Departamentos por Comuna, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarEquipamientoPorDepartamentoURL = "/equipamiento/getAll/";
        public static async Task<List<Equipamiento>> listarEquipamientosPorDepartamentos(int idDepartamento)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarEquipamientoPorDepartamentoURL + idDepartamento);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Equipamiento> equipamientos = await response.Content.ReadAsAsync<List<Equipamiento>>();

                    return equipamientos;
                }
                else
                {
                    return null;
                }
            }
        }


    }

}

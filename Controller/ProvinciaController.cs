﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Controller
{
   public class ProvinciaController
    {
        //Metodo que hace un llamado a la API de tipo GET, la cual recibe una id de Region y devuelve una lista de provincias, que despues se retorna para su visualizacion en el modulo de Departamento.
        private static string listarRegionesURL = "lista/provincias/";
        public static async Task<List<Provincia>> listarProvincias(int idRegion)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarRegionesURL+idRegion);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Provincia> provincias = await response.Content.ReadAsAsync<List<Provincia>>();
                    return provincias;
                }
                else
                {
                    return null;
                }
            }
        }

    }
}

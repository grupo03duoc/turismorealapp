﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using System.Threading.Tasks;
using System.Net.Http;
using Response;


namespace Controller
{
    public class TourController
    {
        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de usuarios, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarToursURL = "tour/get_por_region/";
        public static async Task<List<Tour>> listarTours(int idComuna)
        {

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarToursURL + idComuna);
            {
                if (response.IsSuccessStatusCode)
                {

                    List<Tour> tours = await response.Content.ReadAsAsync<List<Tour>>();

                    return tours;

                }
                else
                {
                    return null;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo POST, el cual permite crear un usuario para luego enviarlo como JSON a la API.
        private static string InsertarTourURL = "tour/create";
        public static async Task<bool> insertarTour(string nombre, string descripcion, int valorPersona, int idRegion)
        {
            TourResponse tour = new TourResponse();
            tour.nombre = nombre;
            tour.descripcion = descripcion;
            tour.valor_persona = valorPersona;
            tour.region = idRegion;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(InsertarTourURL, tour);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private static string InsertarReservaTourURL = "/tour/create/reservaTour";
        public static async Task<bool> InsertarReservaTour(int idTour, int idReserva)
        {
            ReservaTourResponse reservaTour = new ReservaTourResponse();
            reservaTour.tour = idTour;
            reservaTour.reserva = idReserva;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(InsertarReservaTourURL, reservaTour);
            {
  
                if (response.IsSuccessStatusCode)
                {

                    return true;
                }
                else
                {
                  
                    return false;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo PUT, el cual permite actualizar un usuario mediante su id, se envia como JSON a la API.
        private static string EditarTourURL = "tour/update";
        public static async Task<bool> EditarTour(int id, string nombre, string descripcion, int valorPersona, int idRegion)
        {
            TourResponse tour = new TourResponse();

            tour.id = id;
            tour.nombre = nombre;
            tour.descripcion = descripcion;
            tour.valor_persona = valorPersona;
            tour.region = idRegion;
           

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PutAsJsonAsync(EditarTourURL, tour);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


    }
}

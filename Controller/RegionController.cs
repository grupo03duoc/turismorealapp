﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Controller
{
    public class RegionController
    {

        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de regiones, que despues se retorna para su visualizacion en el modulo de Departamento.
        private static string listarRegionesURL = "/lista/regiones";
        public static async Task<List<Region>> listarRegiones()
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarRegionesURL);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Region> regiones = await response.Content.ReadAsAsync<List<Region>>();
                    return regiones;
                }
                else
                {
                    return null;
                }
            }
        }


    }
}

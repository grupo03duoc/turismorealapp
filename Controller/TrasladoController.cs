﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Model;
using Response;

namespace Controller
{
   public class TrasladoController
    {

        private static string trasladoURL = "traslado/create";
        public static async Task<bool> crearTraslado(string fecha, string ubicacion, int idChofer, int idReserva)
        {
            TrasladoResponse traslado = new TrasladoResponse();
            traslado.fecha = fecha;
            traslado.ubicacion = ubicacion;
            traslado.chofer = idChofer;
            traslado.reserva = idReserva;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(trasladoURL, traslado);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }
}

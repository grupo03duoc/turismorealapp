﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Model;
using Response;

namespace Controller
{
    public class EmpresaController
    {
        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de usuarios, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarEmpresasURL = "empresa/getAll";
        public static async Task<List<Empresa>> listarEmpresas()
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarEmpresasURL);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Empresa> empresas = await response.Content.ReadAsAsync<List<Empresa>>();

                    return empresas;

                }
                else
                {
                    return null;
                }
            }
        }

        private static string InsertarEmpresaURL = "empresa/create";
        public static async Task<bool> insertarEmpresa(string nombre, string rubro, string rut)
        {

            Empresa empresa = new Empresa();
            empresa.nombre = nombre;
            empresa.rubro = rubro;
            empresa.rut = rut;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(InsertarEmpresaURL, empresa);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        private static string EditarEmpresaURL = "empresa/update";
        public static async Task<bool> EditarEmpresa(int idEmpresa,string nombre, string rubro, string rut)
        {
            Empresa empresa = new Empresa();
            empresa.id =  idEmpresa;
            empresa.nombre = nombre;
            empresa.rubro = rubro;
            empresa.rut = rut;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PutAsJsonAsync(EditarEmpresaURL, empresa);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Model;
using Response;

namespace Controller
{
  public class DepartamentoController
    {

        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de Departamentos por Comuna, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarDepartamentosURL = " /departamento/all/";
        public static async Task<List<Departamento>> listarDepartamentos(int idComuna)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarDepartamentosURL+idComuna);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Departamento> departamentos = await response.Content.ReadAsAsync<List<Departamento>>();

                    return departamentos;
                }
                else
                {
                    return null;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo POST, el cual permite crear un departamento para luego enviarlo como JSON a la API.
        private static string InsertarDepartamentoURL = "departamento/create";
        public static async Task<bool> insertarDepartamento(string direccion, string caracteristicas, int valorNoche, int dormitorios, int camas, int comuna)
        {

            DepartamentoResponse departamento = new DepartamentoResponse();
            departamento.id = 0;
            departamento.direccion = direccion;
            departamento.caracteristicas = caracteristicas;
            departamento.valorNoche = valorNoche;
            departamento.dormitorios = dormitorios;
            departamento.camas = camas;
            departamento.comuna = comuna;
   

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(InsertarDepartamentoURL, departamento);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }



        //Metodo que hace un llamado a la API de tipo POST, el cual permite crear un departamento para luego enviarlo como JSON a la API.
        private static string VincularDepartamentoEquipamientoURL = "departamento/equipamiento/create";
        public static async Task<bool> vincularDepartamentoEquipamiento(int idDepartamento, int idEquipamiento)
        {

            DepartamentoEquipamientoResponse departamentoEquipamiento = new DepartamentoEquipamientoResponse();
            departamentoEquipamiento.id = idDepartamento;
            departamentoEquipamiento.idEquipamiento = idEquipamiento;    

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(VincularDepartamentoEquipamientoURL, departamentoEquipamiento);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }




        //Metodo que hace un llamado a la API de tipo PUT, el cual permite actualizar un Departamento mediante su id, se envia como JSON a la API.
        private static string EditarDepartamentoURL = "departamento/update";
        public static async Task<bool> EditarDepartamento(int id,string direccion, string caracteristicas, int valorNoche, int dormitorios, int camas, int estado, int comuna)
        {
            DepartamentoResponse departamento = new DepartamentoResponse();
            departamento.id = id;
            departamento.direccion = direccion;
            departamento.caracteristicas = caracteristicas;
            departamento.valorNoche = valorNoche;
            departamento.dormitorios = dormitorios;
            departamento.camas = camas;
            departamento.estado = estado;
            departamento.comuna = comuna;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PutAsJsonAsync(EditarDepartamentoURL, departamento);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo GET, el cual permite cambiar el estado de un usuario mediante su id, se envian los parametros a la API. 
        private static string cambiarEstadoDepartamentoURL = "departamento/disable/";
        public static async Task<bool> cambiarEstadoDepartamento(string estadoIdDepartamento)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(cambiarEstadoDepartamentoURL + estadoIdDepartamento);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
  
                }
                else
                {
                    return false;
                }
            }
        }
    }
}




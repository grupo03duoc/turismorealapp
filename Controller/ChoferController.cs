﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Model;
using Response;

namespace Controller
{
    public class ChoferController
    {

        private static string listarChoferesPorEmpresatURL = "chofer/get_por_empresa/";
        public static async Task<List<Chofer>> listarChoferesPorEmpresa(int idEmpresa)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarChoferesPorEmpresatURL + idEmpresa);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Chofer> choferes = await response.Content.ReadAsAsync<List<Chofer>>();

                    return choferes;

                }
                else
                {
                    return null;
                }
            }
        }

        private static string InsertarChoferURL = "chofer/create";
        public static async Task<bool> insertarChofer(string nombre, string rut, string vehiculo, int idEmpresa)
        {
            ChoferResponse chofer = new ChoferResponse();
            chofer.nombre = nombre;
            chofer.rut = rut;
            chofer.vehiculo = vehiculo;
            chofer.empresa = idEmpresa;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(InsertarChoferURL, chofer);
            {
                Console.WriteLine("Respuesta "+ await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private static string EditarEmpresaURL = "chofer/update";
        public static async Task<bool> EditarChofer(int idChofer, string nombre, string rut, string vehiculo, int idEmpresa)
        {
            ChoferResponse chofer = new ChoferResponse();
            chofer.id = idChofer;
            chofer.nombre = nombre;
            chofer.rut = rut;
            chofer.vehiculo = vehiculo;
            chofer.empresa = idEmpresa;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PutAsJsonAsync(EditarEmpresaURL, chofer);
            {
                Console.WriteLine("Respuesta " + await response.Content.ReadAsStringAsync());
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        }
}

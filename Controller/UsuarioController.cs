﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Model;
using System.Threading.Tasks;
using Response;

namespace Controller
{
    public class UsuarioController
    {
        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de usuarios, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarUsuariosURL = " usuario/users";
        public static async Task<List<Usuario>> listarUsuarios()
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarUsuariosURL);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Usuario> usuarios = await response.Content.ReadAsAsync<List<Usuario>>();
                    return usuarios;
                }
                else
                {
                    return null;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo POST, el cual permite crear un usuario para luego enviarlo como JSON a la API.
        private static string InsertarUsuarioURL = "usuario/create";
        public static async Task<bool> insertarUsuario(string nombre,string username,string correo,string perfil, string password, int habilitado)
        {

            UsuarioResponse usuario = new UsuarioResponse();

            usuario.nombre = nombre;
            usuario.username = username;
            usuario.correo = correo;
            usuario.perfil = perfil;
            usuario.password = password;
            usuario.habilitado= habilitado;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(InsertarUsuarioURL, usuario);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo PUT, el cual permite actualizar un usuario mediante su id, se envia como JSON a la API.
        private static string EditarUsuarioURL = "usuario/update";
        public static async Task<bool> EditarUsuario(int id, string nombre, string username, string correo, string perfil, int habilitado)
        {
            UsuarioResponse usuario = new UsuarioResponse();

            usuario.id = id;
            usuario.nombre = nombre;
            usuario.username = username;
            usuario.correo = correo;
            usuario.perfil = perfil;
            usuario.password = " ";
            usuario.habilitado = habilitado;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PutAsJsonAsync(EditarUsuarioURL, usuario);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo GET, el cual permite cambiar el estado de un usuario mediante su id, se envian los parametros a la API. 
        private static string cambiarEstadoUsuarioURL = "usuario/disable/";
        public static async Task<bool> cambiarEstadoUsuario(string estadoIdUsuario)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(cambiarEstadoUsuarioURL + estadoIdUsuario);
            {
                if (response.IsSuccessStatusCode)
                {

                    return true;
                }
                else
                {
                   return false;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo Post, el cual permite autentificar el usuario a ingresar mediante su usuario y contraseña, se envia como JSON a la API.
        private static string LoginURL = "auth/login";
        public static async Task<bool> LoginUsuario(string username, string password)
        {
            LoginForm login = new LoginForm();

            login.username = username;
            login.password = password;
           
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(LoginURL, login);
            {

                if (response.IsSuccessStatusCode)
                {
                    string respuesta = await response.Content.ReadAsStringAsync();
                    string respuestaSpitUno = respuesta.Split(':')[3];
                    string respuestaSpitDos = respuestaSpitUno.Split(',')[0];
                    string rolUsuario = respuestaSpitDos.Replace("}", "").Replace("]", "").Replace(@"""", "");

                    if (rolUsuario =="ROLE_ADMIN")
                    {
                        return true;
                    }
                    return false;                         
                }
                else
                {
                    return false;
                }
            }
        }
    }
}

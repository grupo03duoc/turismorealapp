﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Model;
using System.Net.Http;

namespace Controller
{
    public class RestService
    {
        //Se inicializa Api cliente para trabajar solamente con JSON, ademas de agregar la direccion de la API
        public static HttpClient ApiService  ()
        {
            HttpClient ApiClient = new HttpClient();
            ApiClient.BaseAddress = new Uri("http://turismoreal.us-east-1.elasticbeanstalk.com/");
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            return ApiClient;
        }
    }
}

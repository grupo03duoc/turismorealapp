﻿using System;
using System.Collections.Generic;
using System.Text;
using Model;
using System.Threading.Tasks;
using System.Net.Http;
using Response;

namespace Controller
{
   public class ReservaController
    {

        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de usuarios, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarReservasURL = "reserva/getAll";
        public static async Task<List<Reserva>> listarReservas()
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarReservasURL);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Reserva> reservas = await response.Content.ReadAsAsync<List<Reserva>>();
                    return reservas;
                }
                else
                {
                    return null;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo POST, el cual permite crear una reserva para luego enviarlo como JSON a la API.
        private static string InsertarReservaURL = "reserva/create";
        public static async Task<int> insertarReserva(string fechaInicio, string fechaTermino,int personas, int idUsuario, int idDepartamento, int total)
        {

            ReservaResponse reserva = new ReservaResponse();

            reserva.fechaInicio = fechaInicio;
            reserva.fechaTermino = fechaTermino;
            reserva.personas = personas;
            reserva.usuario = idUsuario;
            reserva.departamento = idDepartamento;
            reserva.total = total;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(InsertarReservaURL, reserva);
            {
                if (response.IsSuccessStatusCode)
                {
                    string respuesta = await response.Content.ReadAsStringAsync();
                    string idStringReserva = respuesta.Split(':')[3];
                    int idReserva= int.Parse((idStringReserva.Replace("}", "")));

                    return idReserva;
                }
                else
                {
                    return 0;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de usuarios, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarReservasPorUsuarioURL = "reserva/getAllPorUsuario/";
        public static async Task<List<Reserva>> listarReservasPorUsuario(int idUsuario)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarReservasPorUsuarioURL + idUsuario);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Reserva> reservas = await response.Content.ReadAsAsync<List<Reserva>>();
                    return reservas;
                }
                else
                {
                    return null;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de usuarios, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarReservasPorIdURL = "reserva/get/";
        public static async Task<List<Reserva>> listarReservasPorID(int idReserva)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarReservasPorIdURL + idReserva);
            {
                if (response.IsSuccessStatusCode)
                {
                    List<Reserva> reservas = await response.Content.ReadAsAsync<List<Reserva>>();
                    return reservas;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
        
 
        private static string checkInReservaURL = "checkin/create";
        public static async Task<bool> checkInReserva(int idReserva, int conforme)
        {
            CheckIn checkIn = new CheckIn();
            checkIn.nombreEmpleado = "Administrador";
            checkIn.conforme = conforme;
            checkIn.idReserva = idReserva;

            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(checkInReservaURL, checkIn);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de usuarios, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string checkOutReservaURL = "checkout/create";
        public static async Task<bool> checkOutReserva(int idReserva, int multa)
        {

            CheckOut checkout = new CheckOut();
            checkout.nombreEmpleado = "Administrador";
            checkout.multa = multa;
            checkout.idReserva = idReserva;


            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.PostAsJsonAsync(checkOutReservaURL, checkout);
            {
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de usuarios, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarReservasCheckOutURL = "checkout/get/";
        public static async Task<CheckOut> listarReservasCheckOut(int idReserva)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarReservasCheckOutURL + idReserva);
            {
                if (response.IsSuccessStatusCode)
                {
                   
                    CheckOut checkout =  await response.Content.ReadAsAsync<CheckOut>();

                    return checkout;
                  
                }
                else
                {
                    return null;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de usuarios, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarReservasCheckInURL = "checkin/get/";
        public static async Task<CheckIn> listarReservasCheckIn(int idReserva)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarReservasCheckInURL + idReserva);
            {
                if (response.IsSuccessStatusCode)
                {
                 
                    CheckIn checkin = await response.Content.ReadAsAsync<CheckIn>();
                    return checkin;       
                }
                else
                {
                    return null;
                }
            }
        }

        //Metodo que hace un llamado a la API de tipo GET, la cual devuelve una lista de usuarios, que despues se retorna para su visualizacion en el modulo de usuarios.
        private static string listarReservasPorDepartamentoURL = "reserva/getAllPorDepartamento/";
        public static async Task<List<Reserva>> listarReservasPorDepartamento(int idDepartamento)
        {
            HttpClient apiCall = RestService.ApiService();

            HttpResponseMessage response = await apiCall.GetAsync(listarReservasPorDepartamentoURL + idDepartamento);
            {
                if (response.IsSuccessStatusCode)
                {
               
                    List<Reserva> reservas = await response.Content.ReadAsAsync<List<Reserva>>();
                    return reservas;
                }
                else
                {
                    return null;
                }
            }
        }

    }
}


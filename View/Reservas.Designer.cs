﻿namespace View
{
    partial class Reservas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvReservas = new System.Windows.Forms.DataGridView();
            this.label10 = new System.Windows.Forms.Label();
            this.lblReservas = new System.Windows.Forms.Label();
            this.lblCheckOut = new System.Windows.Forms.Label();
            this.txtMulta = new System.Windows.Forms.TextBox();
            this.lblMulta = new System.Windows.Forms.Label();
            this.btnCheckOut = new System.Windows.Forms.Button();
            this.lblCheckIn = new System.Windows.Forms.Label();
            this.dgvCheckOut = new System.Windows.Forms.DataGridView();
            this.dgvCheckIn = new System.Windows.Forms.DataGridView();
            this.lblConforme = new System.Windows.Forms.Label();
            this.cbConforme = new System.Windows.Forms.ComboBox();
            this.btnCheckIn = new System.Windows.Forms.Button();
            this.btnMenuReservas = new System.Windows.Forms.Button();
            this.lblSeleccionarUsuarioForm = new System.Windows.Forms.Label();
            this.lblBuscador = new System.Windows.Forms.Label();
            this.txtBuscador = new System.Windows.Forms.TextBox();
            this.dgvUsuarios = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReservas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCheckOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCheckIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvReservas
            // 
            this.dgvReservas.AllowUserToAddRows = false;
            this.dgvReservas.AllowUserToDeleteRows = false;
            this.dgvReservas.AllowUserToOrderColumns = true;
            this.dgvReservas.AllowUserToResizeColumns = false;
            this.dgvReservas.AllowUserToResizeRows = false;
            this.dgvReservas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvReservas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReservas.Location = new System.Drawing.Point(20, 256);
            this.dgvReservas.Name = "dgvReservas";
            this.dgvReservas.Size = new System.Drawing.Size(703, 328);
            this.dgvReservas.TabIndex = 1;
            this.dgvReservas.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvReservas_CellClick);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label10.Location = new System.Drawing.Point(496, 358);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 29);
            this.label10.TabIndex = 69;
            // 
            // lblReservas
            // 
            this.lblReservas.AutoSize = true;
            this.lblReservas.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReservas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblReservas.Location = new System.Drawing.Point(320, 221);
            this.lblReservas.Name = "lblReservas";
            this.lblReservas.Size = new System.Drawing.Size(130, 31);
            this.lblReservas.TabIndex = 84;
            this.lblReservas.Text = "Reservas";
            // 
            // lblCheckOut
            // 
            this.lblCheckOut.AutoSize = true;
            this.lblCheckOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckOut.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCheckOut.Location = new System.Drawing.Point(849, 253);
            this.lblCheckOut.Name = "lblCheckOut";
            this.lblCheckOut.Size = new System.Drawing.Size(127, 29);
            this.lblCheckOut.TabIndex = 107;
            this.lblCheckOut.Text = "Check-Out";
            // 
            // txtMulta
            // 
            this.txtMulta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMulta.Location = new System.Drawing.Point(808, 368);
            this.txtMulta.Name = "txtMulta";
            this.txtMulta.Size = new System.Drawing.Size(144, 23);
            this.txtMulta.TabIndex = 106;
            // 
            // lblMulta
            // 
            this.lblMulta.AutoSize = true;
            this.lblMulta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMulta.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblMulta.Location = new System.Drawing.Point(756, 374);
            this.lblMulta.Name = "lblMulta";
            this.lblMulta.Size = new System.Drawing.Size(46, 17);
            this.lblMulta.TabIndex = 105;
            this.lblMulta.Text = "Multa:";
            // 
            // btnCheckOut
            // 
            this.btnCheckOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnCheckOut.FlatAppearance.BorderSize = 0;
            this.btnCheckOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckOut.ForeColor = System.Drawing.Color.White;
            this.btnCheckOut.Location = new System.Drawing.Point(971, 356);
            this.btnCheckOut.Name = "btnCheckOut";
            this.btnCheckOut.Size = new System.Drawing.Size(100, 35);
            this.btnCheckOut.TabIndex = 104;
            this.btnCheckOut.Text = "Check-Out";
            this.btnCheckOut.UseVisualStyleBackColor = false;
            this.btnCheckOut.Click += new System.EventHandler(this.btnCheckOut_Click);
            // 
            // lblCheckIn
            // 
            this.lblCheckIn.AutoSize = true;
            this.lblCheckIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckIn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCheckIn.Location = new System.Drawing.Point(858, 422);
            this.lblCheckIn.Name = "lblCheckIn";
            this.lblCheckIn.Size = new System.Drawing.Size(108, 29);
            this.lblCheckIn.TabIndex = 103;
            this.lblCheckIn.Text = "Check-In";
            // 
            // dgvCheckOut
            // 
            this.dgvCheckOut.AllowUserToAddRows = false;
            this.dgvCheckOut.AllowUserToDeleteRows = false;
            this.dgvCheckOut.AllowUserToOrderColumns = true;
            this.dgvCheckOut.AllowUserToResizeColumns = false;
            this.dgvCheckOut.AllowUserToResizeRows = false;
            this.dgvCheckOut.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCheckOut.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCheckOut.Location = new System.Drawing.Point(759, 285);
            this.dgvCheckOut.Name = "dgvCheckOut";
            this.dgvCheckOut.Size = new System.Drawing.Size(312, 58);
            this.dgvCheckOut.TabIndex = 102;
            // 
            // dgvCheckIn
            // 
            this.dgvCheckIn.AllowUserToAddRows = false;
            this.dgvCheckIn.AllowUserToDeleteRows = false;
            this.dgvCheckIn.AllowUserToOrderColumns = true;
            this.dgvCheckIn.AllowUserToResizeColumns = false;
            this.dgvCheckIn.AllowUserToResizeRows = false;
            this.dgvCheckIn.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCheckIn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCheckIn.Location = new System.Drawing.Point(759, 454);
            this.dgvCheckIn.Name = "dgvCheckIn";
            this.dgvCheckIn.Size = new System.Drawing.Size(312, 58);
            this.dgvCheckIn.TabIndex = 101;
            // 
            // lblConforme
            // 
            this.lblConforme.AutoSize = true;
            this.lblConforme.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConforme.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblConforme.Location = new System.Drawing.Point(756, 553);
            this.lblConforme.Name = "lblConforme";
            this.lblConforme.Size = new System.Drawing.Size(73, 17);
            this.lblConforme.TabIndex = 100;
            this.lblConforme.Text = "Conforme:";
            // 
            // cbConforme
            // 
            this.cbConforme.FormattingEnabled = true;
            this.cbConforme.Location = new System.Drawing.Point(839, 549);
            this.cbConforme.Name = "cbConforme";
            this.cbConforme.Size = new System.Drawing.Size(66, 21);
            this.cbConforme.TabIndex = 99;
            // 
            // btnCheckIn
            // 
            this.btnCheckIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnCheckIn.FlatAppearance.BorderSize = 0;
            this.btnCheckIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheckIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckIn.ForeColor = System.Drawing.Color.White;
            this.btnCheckIn.Location = new System.Drawing.Point(971, 535);
            this.btnCheckIn.Name = "btnCheckIn";
            this.btnCheckIn.Size = new System.Drawing.Size(100, 35);
            this.btnCheckIn.TabIndex = 98;
            this.btnCheckIn.Text = "Check-In";
            this.btnCheckIn.UseVisualStyleBackColor = false;
            this.btnCheckIn.Click += new System.EventHandler(this.btnCheckIn_Click);
            // 
            // btnMenuReservas
            // 
            this.btnMenuReservas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnMenuReservas.FlatAppearance.BorderSize = 0;
            this.btnMenuReservas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenuReservas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMenuReservas.ForeColor = System.Drawing.Color.White;
            this.btnMenuReservas.Location = new System.Drawing.Point(876, 64);
            this.btnMenuReservas.Name = "btnMenuReservas";
            this.btnMenuReservas.Size = new System.Drawing.Size(100, 35);
            this.btnMenuReservas.TabIndex = 108;
            this.btnMenuReservas.Text = "Crear Reserva";
            this.btnMenuReservas.UseVisualStyleBackColor = false;
            this.btnMenuReservas.Click += new System.EventHandler(this.btnMenuReservas_Click);
            // 
            // lblSeleccionarUsuarioForm
            // 
            this.lblSeleccionarUsuarioForm.AutoSize = true;
            this.lblSeleccionarUsuarioForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeleccionarUsuarioForm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSeleccionarUsuarioForm.Location = new System.Drawing.Point(310, 24);
            this.lblSeleccionarUsuarioForm.Name = "lblSeleccionarUsuarioForm";
            this.lblSeleccionarUsuarioForm.Size = new System.Drawing.Size(174, 24);
            this.lblSeleccionarUsuarioForm.TabIndex = 109;
            this.lblSeleccionarUsuarioForm.Text = "Seleccione Usuario";
            // 
            // lblBuscador
            // 
            this.lblBuscador.AutoSize = true;
            this.lblBuscador.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscador.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblBuscador.Location = new System.Drawing.Point(217, 57);
            this.lblBuscador.Name = "lblBuscador";
            this.lblBuscador.Size = new System.Drawing.Size(56, 17);
            this.lblBuscador.TabIndex = 145;
            this.lblBuscador.Text = "Buscar:";
            // 
            // txtBuscador
            // 
            this.txtBuscador.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscador.Location = new System.Drawing.Point(279, 51);
            this.txtBuscador.Name = "txtBuscador";
            this.txtBuscador.Size = new System.Drawing.Size(242, 23);
            this.txtBuscador.TabIndex = 144;
            this.txtBuscador.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBuscador_KeyPress);
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.AllowUserToAddRows = false;
            this.dgvUsuarios.AllowUserToDeleteRows = false;
            this.dgvUsuarios.AllowUserToOrderColumns = true;
            this.dgvUsuarios.AllowUserToResizeColumns = false;
            this.dgvUsuarios.AllowUserToResizeRows = false;
            this.dgvUsuarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsuarios.Location = new System.Drawing.Point(20, 81);
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.ReadOnly = true;
            this.dgvUsuarios.Size = new System.Drawing.Size(703, 96);
            this.dgvUsuarios.TabIndex = 143;
            this.dgvUsuarios.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUsuarios_CellClick);
            // 
            // Reservas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 606);
            this.Controls.Add(this.lblBuscador);
            this.Controls.Add(this.txtBuscador);
            this.Controls.Add(this.dgvUsuarios);
            this.Controls.Add(this.lblSeleccionarUsuarioForm);
            this.Controls.Add(this.btnMenuReservas);
            this.Controls.Add(this.lblCheckOut);
            this.Controls.Add(this.txtMulta);
            this.Controls.Add(this.lblMulta);
            this.Controls.Add(this.btnCheckOut);
            this.Controls.Add(this.lblCheckIn);
            this.Controls.Add(this.dgvCheckOut);
            this.Controls.Add(this.dgvCheckIn);
            this.Controls.Add(this.lblConforme);
            this.Controls.Add(this.cbConforme);
            this.Controls.Add(this.btnCheckIn);
            this.Controls.Add(this.lblReservas);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dgvReservas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Reservas";
            this.Text = "Reservas";
            this.Load += new System.EventHandler(this.Reservas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvReservas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCheckOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCheckIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvReservas;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblReservas;
        private System.Windows.Forms.Label lblCheckOut;
        public System.Windows.Forms.TextBox txtMulta;
        private System.Windows.Forms.Label lblMulta;
        private System.Windows.Forms.Button btnCheckOut;
        private System.Windows.Forms.Label lblCheckIn;
        private System.Windows.Forms.DataGridView dgvCheckOut;
        private System.Windows.Forms.DataGridView dgvCheckIn;
        private System.Windows.Forms.Label lblConforme;
        private System.Windows.Forms.ComboBox cbConforme;
        private System.Windows.Forms.Button btnCheckIn;
        private System.Windows.Forms.Button btnMenuReservas;
        private System.Windows.Forms.Label lblSeleccionarUsuarioForm;
        private System.Windows.Forms.Label lblBuscador;
        public System.Windows.Forms.TextBox txtBuscador;
        private System.Windows.Forms.DataGridView dgvUsuarios;
    }
}
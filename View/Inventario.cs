﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controller;
using Model;
using Response;

namespace View
{
    public partial class Inventario : Form
    {
        public Inventario()
        {
            InitializeComponent();
        }

        public async Task cargarRegionesAsync()
        {
            if (await RegionController.listarRegiones()!=null)
            {
                cbRegion.DisplayMember = "nombre";
                cbRegion.ValueMember = "id";
                cbRegion.DataSource = await RegionController.listarRegiones();
            }

        }

        private async void listarDepartamentos(int idComuna)
        {

            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("DIRECCION");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("CARACTERISTICAS");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("VALOR NOCHE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("DORMITORIOS");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("CAMAS");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("COMUNA");
            dataTable.Columns.Add(dataColumn);

            if (await DepartamentoController.listarDepartamentos(idComuna)!=null) {
                foreach (Departamento departamento in await DepartamentoController.listarDepartamentos(idComuna))
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = departamento.Id;
                    dataRow[1] = departamento.Direccion;
                    dataRow[2] = departamento.Caracteristicas;
                    dataRow[3] = departamento.ValorNoche;
                    dataRow[4] = departamento.Dormitorios;
                    dataRow[5] = departamento.Camas;
                    dataRow[6] = departamento.Comuna.Nombre;
                    dataTable.Rows.Add(dataRow);


                }

                dgvDepartamento.DataSource = dataTable;
                dgvDepartamento.Columns[0].Visible = false;
            }
        }

        private async void btnGuardar_Click(object sender, EventArgs e)
        {
            try{
               if(await EquipamientoController.insertarEquipamiento(txtNombre.Text, int.Parse(txtInventario.Text), int.Parse(txtValorUnitario.Text)))
                {
                    MessageBox.Show("Inventario insertado Correctamente");
                    listarEquipamientos();
                    limpiarFormInventario();

                }else
                {
                    MessageBox.Show("No se pudo insertar el inventario, intentar nuevamente");
                    listarEquipamientos();
                    limpiarFormInventario();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor verificar que ningun campo este vacio y con caracteres no correspondientes");
            }
        }

        //Metodo que permite limpiar todos los combobox y textbox.
        private void limpiarFormInventario()
        {
            txtNombre.Clear();
            txtInventario.Clear();
            txtValorUnitario.Clear();

        }


        private async void listarEquipamientos()
        {

            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("CANTIDAD");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("VALOR UNITARIO");
            dataTable.Columns.Add(dataColumn);

            if (await EquipamientoController.listarEquipamientos()!=null) {
                foreach (Equipamiento equipamiento in await EquipamientoController.listarEquipamientos())
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = equipamiento.Id;
                    dataRow[1] = equipamiento.Nombre;
                    dataRow[2] = equipamiento.Inventario;
                    dataRow[3] = equipamiento.ValorUnitario;
                    dataTable.Rows.Add(dataRow);

                }
                dgvInventarioDisponible.DataSource = dataTable;
                dgvInventarioDisponible.Columns[0].Visible = false;
            }
        }

        private async void Inventario_Load(object sender, EventArgs e)
        {
            await cargarRegionesAsync();
            listarEquipamientos();
        }

        private async void cbRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (await ProvinciaController.listarProvincias(int.Parse(cbRegion.SelectedValue.ToString()))!=null) {
                cbProvincia.DisplayMember = "nombre";
                cbProvincia.ValueMember = "id";
                cbProvincia.DataSource = await ProvinciaController.listarProvincias(int.Parse(cbRegion.SelectedValue.ToString()));
            }
          
        }

        private async void cbProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (await ComunaController.listarComunas(int.Parse(cbProvincia.SelectedValue.ToString()))!=null) {
                cbComuna.DisplayMember = "nombre";
                cbComuna.ValueMember = "id";
                cbComuna.DataSource = await ComunaController.listarComunas(int.Parse(cbProvincia.SelectedValue.ToString()));
            }
        }

        private void cbComuna_SelectedIndexChanged(object sender, EventArgs e)
        {
            listarDepartamentos(int.Parse(cbComuna.SelectedValue.ToString()));
        }

        private async void btnVincularEquipamientoDepartamento_Click(object sender, EventArgs e)
        {
            if (dgvDepartamento.SelectedRows.Count > 0 && dgvInventarioDisponible.SelectedRows.Count > 0)
            {

                int idDepartamento = int.Parse(dgvDepartamento.CurrentRow.Cells["ID"].Value.ToString());
                int idInventarioDisponible = int.Parse(dgvInventarioDisponible.CurrentRow.Cells["ID"].Value.ToString());

                if (await DepartamentoController.vincularDepartamentoEquipamiento(idDepartamento, idInventarioDisponible)) {
                    
                    MessageBox.Show("Equipamiento agregado a departamento");
                    listarDepartamentos(int.Parse(cbComuna.SelectedValue.ToString()));

                }
                else
                {
                    MessageBox.Show("Equipamiento no agregado a departamento, intente nuevamente");
                    listarDepartamentos(int.Parse(cbComuna.SelectedValue.ToString()));
                }

            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila en Departamento y otra en Equipamiento");

            }
        }

        private async void btnEditar_Click(object sender, EventArgs e)
        {
            if (dgvInventarioDisponible.SelectedRows.Count > 0)
            {

                int idInventarioDisponible = int.Parse(dgvInventarioDisponible.CurrentRow.Cells["ID"].Value.ToString());

                if (await EquipamientoController.eliminarEquipamiento(idInventarioDisponible))
                {
                    MessageBox.Show("Equipamiento eliminado exitosamente");
                    listarEquipamientos();

                }else
                {
                    MessageBox.Show("No se pudo eliminar el equipamiento, intente nuevamente");
                    listarEquipamientos();
                }

            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila en Equipamiento a Eliminar");

            }
        }

        private void dgvInventario_DragEnter(object sender, DragEventArgs e)
        {

        }

        private async void dgvDepartamento_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgvDepartamento.SelectedRows.Count > 0)
            {

                if (await EquipamientoController.listarEquipamientosPorDepartamentos(int.Parse(dgvDepartamento.CurrentRow.Cells["ID"].Value.ToString()))!=null) {
                    List<Equipamiento> quipamientos =
                    await EquipamientoController.listarEquipamientosPorDepartamentos(int.Parse(dgvDepartamento.CurrentRow.Cells["ID"].Value.ToString()));

                    DataTable dataTableIventario = new DataTable();
                    DataColumn dataColumnInventario;
                    dataColumnInventario = new DataColumn("ID");
                    dataTableIventario.Columns.Add(dataColumnInventario);
                    dataColumnInventario = new DataColumn("NOMBRE");
                    dataTableIventario.Columns.Add(dataColumnInventario);
                    dataColumnInventario = new DataColumn("CANTIDAD");
                    dataTableIventario.Columns.Add(dataColumnInventario);
                    dataColumnInventario = new DataColumn("VALOR UNITARIO");
                    dataTableIventario.Columns.Add(dataColumnInventario);

                    foreach (Equipamiento equipamiento in quipamientos)
                    {

                        DataRow dataRowInventario = dataTableIventario.NewRow();
                        dataRowInventario[0] = equipamiento.Id;
                        dataRowInventario[1] = equipamiento.Nombre;
                        dataRowInventario[2] = equipamiento.Inventario;
                        dataRowInventario[3] = equipamiento.ValorUnitario;
                        dataTableIventario.Rows.Add(dataRowInventario);

                    }

                    dgvInventario.DataSource = dataTableIventario;
                    dgvInventario.Columns[0].Visible = false;
                }

            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila para ver el equipamiento del departamento seleccionado");
            }
        }


      
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controller;
using Model;
using Response;

namespace View
{
    public partial class

     
        Reservas : Form
    {

        public Reservas()
        {
            InitializeComponent();
        }

     
        private int idUsuario;
   

        private async void ListarUsuariosAsync()
        {
            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE USUARIO");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("CORREO");
            dataTable.Columns.Add(dataColumn);


            if (await UsuarioController.listarUsuarios() != null)
            {
                foreach (Usuario usuario in await UsuarioController.listarUsuarios())
                {
                    foreach (Perfil perfil in usuario.Perfil)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow[0] = usuario.Id;
                        dataRow[1] = usuario.Nombre;
                        dataRow[2] = usuario.Username;
                        dataRow[3] = usuario.Correo;

                        dataTable.Rows.Add(dataRow);
                    }
                }

                dgvUsuarios.DataSource = dataTable;
                dgvUsuarios.Columns[0].Visible = false;
            }

        }


        private async void ListarReservas(int idUsuario)
        {
            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("FECHA INICIO");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("FECHA TERMINO");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("PERSONAS");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("TOTAL");
            dataTable.Columns.Add(dataColumn);

            if (await ReservaController.listarReservasPorUsuario(idUsuario) != null)
            {
                foreach (Reserva reserva in await ReservaController.listarReservasPorUsuario(idUsuario))
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = reserva.Id;
                    dataRow[1] = reserva.FechaInicio;
                    dataRow[2] = reserva.FechaTermino;
                    dataRow[3] = reserva.Personas;
                    dataRow[4] = reserva.Total;

                    dataTable.Rows.Add(dataRow);

                }
                dgvReservas.DataSource = dataTable;
                dgvReservas.Columns[0].Visible = false;
            }
        }

        private async void Reservas_Load(object sender, EventArgs e)
        {
            ListarUsuariosAsync();
            cbConforme.Items.Add("Si");
            cbConforme.Items.Add("No");
        }

        private async void dgvReservas_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgvReservas.SelectedRows.Count > 0)
            {

                DataTable dataTableCheckOut = new DataTable();
                DataColumn dataColumnCheckOut;
                dataColumnCheckOut = new DataColumn("ID");
                dataTableCheckOut.Columns.Add(dataColumnCheckOut);
                dataColumnCheckOut = new DataColumn("NOMBRE EMPLEADO");
                dataTableCheckOut.Columns.Add(dataColumnCheckOut);
                dataColumnCheckOut = new DataColumn("MULTA");
                dataTableCheckOut.Columns.Add(dataColumnCheckOut);

                DataTable dataTable = new DataTable();
                DataColumn dataColumn;
                dataColumn = new DataColumn("ID");
                dataTable.Columns.Add(dataColumn);
                dataColumn = new DataColumn("NOMBRE EMPLEADO");
                dataTable.Columns.Add(dataColumn);
                dataColumn = new DataColumn("CONFORME");
                dataTable.Columns.Add(dataColumn);

 
                if (await ReservaController.listarReservasCheckOut(int.Parse(dgvReservas.CurrentRow.Cells["ID"].Value.ToString())) != null )
                {


                    CheckOut checkOut = await ReservaController.listarReservasCheckOut(int.Parse(dgvReservas.CurrentRow.Cells["ID"].Value.ToString()));

                    DataRow dataRowCheckOut = dataTableCheckOut.NewRow();
                    dataRowCheckOut[0] = checkOut.id;
                    dataRowCheckOut[1] = checkOut.nombreEmpleado;
                    dataRowCheckOut[2] = checkOut.multa;

                    dataTableCheckOut.Rows.Add(dataRowCheckOut);
 
                }
                dgvCheckOut.DataSource = dataTableCheckOut;
                dgvCheckOut.Columns[0].Visible = false;


                if (await ReservaController.listarReservasCheckIn(int.Parse(dgvReservas.CurrentRow.Cells["ID"].Value.ToString())) != null)
                {

                    CheckIn checkIn = await ReservaController.listarReservasCheckIn(int.Parse(dgvReservas.CurrentRow.Cells["ID"].Value.ToString()));

                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = checkIn.id;
                    dataRow[1] = checkIn.nombreEmpleado;

                    if (checkIn.conforme == 0)
                    {
                        dataRow[2] = "Si";
                    }
                    else
                    {
                        dataRow[2] = "No";
                    }

                    dataTable.Rows.Add(dataRow);
                  
                }
                dgvCheckIn.DataSource = dataTable;
                dgvCheckIn.Columns[0].Visible = false;

            }
            else
            {
                
                MessageBox.Show("Debe seleccionar una fila para ver el check in o check out de la reserva");
            }

        }


        private async void btnCheckIn_Click(object sender, EventArgs e)
        {
            try
            {
                int idConforme = 0;
                if (dgvReservas.SelectedRows.Count > 0)
                {
                    int idReserva = int.Parse(dgvReservas.CurrentRow.Cells["ID"].Value.ToString());
                    string conforme = cbConforme.SelectedItem.ToString();

                    if (conforme == "Si")
                    {
                        idConforme = 1;

                    }
                    if (await ReservaController.checkInReserva(idReserva, idConforme))
                    {
                        MessageBox.Show("Check In Realizado con Exito");
                        ListarReservas(idUsuario);
                    }
                    else
                    {
                        MessageBox.Show("Check In fallido, intente nuevamente");
                        ListarReservas(idUsuario);

                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor verificar que ningun campo este vacio y con caracteres no correspondientes");
            }

        }

        private async void btnCheckOut_Click(object sender, EventArgs e)
        {
            try
            {
                int multa = 0;
                if (dgvReservas.SelectedRows.Count > 0)
                {
                    int idReserva = int.Parse(dgvReservas.CurrentRow.Cells["ID"].Value.ToString());
                    multa = int.Parse(txtMulta.Text);

                    if (await ReservaController.checkOutReserva(idReserva, multa))
                    {
                        MessageBox.Show("Check Out Realizado con Exito");
                        ListarReservas(idUsuario);
                    }
                    else
                    {
                        MessageBox.Show("Check Out fallido, intente nuevamente");
                        ListarReservas(idUsuario);

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor verificar que ningun campo este vacio y con caracteres no correspondientes");
            }
        }

        private void btnMenuReservas_Click(object sender, EventArgs e)
        {

            txtMulta.Visible = false;
            lblReservas.Visible = false;
            lblConforme.Visible = false;
            lblCheckIn.Visible = false;
            lblCheckOut.Visible = false;
            dgvCheckIn.Visible = false;
            dgvCheckOut.Visible = false;
            dgvReservas.Visible = false;
            btnMenuReservas.Visible = false;
            btnCheckIn.Visible = false;
            btnCheckOut.Visible = false;
            lblMulta.Visible = false;
            cbConforme.Visible = false;
            dgvUsuarios.Visible = false;
            txtBuscador.Visible = false;
            lblSeleccionarUsuarioForm.Visible = false;
            lblBuscador.Visible = false;
            FormReservas form = new FormReservas();
            form.TopLevel = false;
            this.Controls.Add(form);
            form.Show();


        }

        private void txtBuscador_KeyPress(object sender, KeyPressEventArgs e)
        {
            (dgvUsuarios.DataSource as DataTable).DefaultView.RowFilter = string.Format("NOMBRE LIKE'{0}%'", txtBuscador.Text);
        }

        private async void dgvUsuarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvUsuarios.SelectedRows.Count > 0)
            {
                 idUsuario = int.Parse(dgvUsuarios.CurrentRow.Cells["ID"].Value.ToString());
                 ListarReservas(idUsuario);
               
              
            }
        }
    }

}

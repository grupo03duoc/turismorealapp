﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Controller;

namespace View
{
    public partial class MenuInicio : Form
    {
        public MenuInicio()
        {
            InitializeComponent();
        
        }

        int LX, LY;

        //Se agregan DLL para configurar form y asi poder arrastrar la ventana de un lugar a otro
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wwmsg, int wparam, int lparam);

        //Se configura barra lateral para cambiar el tamaño una vez se presione el boton.
        private void btnBarraLateral_Click(object sender, EventArgs e)
        {
            if (barraLateral.Width == 200)
            {
                barraLateral.Width = 50;
            }
            else
            {
                barraLateral.Width = 200;
            }
        }

        //Se configura el boton cerrar para cerrar la aplicacion una vez se presione el boton.
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de cerrar la aplicacion?", "Alerta", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Exit();
            } 
        }

        //Se configura el boton maximizar para maximizar la aplicacion una vez se presione el boton.
        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            LX = this.Location.X;
            LY = this.Location.Y;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            btnRestaurar.Visible = true;
            btnMaximizar.Visible = false;
        }

        //Se configura el boton restaurar para restaurar la aplicacion una vez se presione el boton.
        private void btnRestaurar_Click(object sender, EventArgs e)
        {
         
            this.Size = new Size(1300, 650);
            this.Location = new Point(LX, LY);
            btnRestaurar.Visible = false;
            btnMaximizar.Visible = true;

        }

        //Se configura el boton minimizar para minimizar la aplicacion una vez se presione el boton.
        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }


        //Se configura barra horizontal para poder arrastrar la ventana de un lugar a otro
        private void barraHorizontal_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        //Metodo para abrir un nuevo form, el cual recibe como parametro un objeto form, esto permite la navegacion entre pestañas de la app 
        public void AbrirForm(object form)
        {
            if (this.panelContenedor.Controls.Count > 0 )
            {
                this.panelContenedor.Controls.RemoveAt(0);

                Form fhR = form as Form;
                fhR.TopLevel = false;
                fhR.Dock = DockStyle.Fill;
                this.panelContenedor.Controls.Add(fhR);
                this.panelContenedor.Tag = fhR;
                fhR.Show();
            }
        }

        //Metodo para abrir el form reservas una vez se presiona el boton Reservas.
        private void btnReservas_Click(object sender, EventArgs e)
        {
            AbrirForm(new Reservas());
        }

        private void tHoraFecha_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToLongTimeString();
            lblFecha.Text = DateTime.Now.ToLongDateString();
        }
      

        private void btnDepartamentos_Click(object sender, EventArgs e)
        {
            AbrirForm(new Departamentos());
        }


        private void btnInventario_Click(object sender, EventArgs e)
        {
            AbrirForm(new Inventario());
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            AbrirForm(new Usuarios());
        }

        private void btnTours_Click(object sender, EventArgs e)
        {
            AbrirForm(new Tours());
        }

        private void btnChoferes_Click(object sender, EventArgs e)
        {
            AbrirForm(new Choferes());
        }

        private void btnEmpresas_Click(object sender, EventArgs e)
        {
            AbrirForm(new Empresas());
        }

        private void btnReportes_Click(object sender, EventArgs e)
        {
            AbrirForm(new Reportes());
        }

    }
}

﻿namespace View
{
    partial class Departamentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDormitorios = new System.Windows.Forms.Label();
            this.lblValorNoche = new System.Windows.Forms.Label();
            this.lblCaracteristicas = new System.Windows.Forms.Label();
            this.lblDireccion = new System.Windows.Forms.Label();
            this.txtDormitorios = new System.Windows.Forms.TextBox();
            this.txtValorNoche = new System.Windows.Forms.TextBox();
            this.txtCaracteristicas = new System.Windows.Forms.TextBox();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.dgvDepartamentos = new System.Windows.Forms.DataGridView();
            this.lblRegion = new System.Windows.Forms.Label();
            this.lblComuna = new System.Windows.Forms.Label();
            this.lblProvincia = new System.Windows.Forms.Label();
            this.cbRegion = new System.Windows.Forms.ComboBox();
            this.cbComuna = new System.Windows.Forms.ComboBox();
            this.cbProvincia = new System.Windows.Forms.ComboBox();
            this.lblCamas = new System.Windows.Forms.Label();
            this.txtCamas = new System.Windows.Forms.TextBox();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.lblDepartamentos = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.lblCrearDepartamento = new System.Windows.Forms.Label();
            this.btnAgregarDepartamentos = new System.Windows.Forms.Button();
            this.btnEditarDepartamento = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDepartamentos)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDormitorios
            // 
            this.lblDormitorios.AutoSize = true;
            this.lblDormitorios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDormitorios.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDormitorios.Location = new System.Drawing.Point(736, 190);
            this.lblDormitorios.Name = "lblDormitorios";
            this.lblDormitorios.Size = new System.Drawing.Size(84, 17);
            this.lblDormitorios.TabIndex = 22;
            this.lblDormitorios.Text = "Dormitorios:";
            this.lblDormitorios.Visible = false;
            // 
            // lblValorNoche
            // 
            this.lblValorNoche.AutoSize = true;
            this.lblValorNoche.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorNoche.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblValorNoche.Location = new System.Drawing.Point(736, 161);
            this.lblValorNoche.Name = "lblValorNoche";
            this.lblValorNoche.Size = new System.Drawing.Size(90, 17);
            this.lblValorNoche.TabIndex = 21;
            this.lblValorNoche.Text = "Valor Noche:";
            this.lblValorNoche.Visible = false;
            // 
            // lblCaracteristicas
            // 
            this.lblCaracteristicas.AutoSize = true;
            this.lblCaracteristicas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCaracteristicas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCaracteristicas.Location = new System.Drawing.Point(735, 132);
            this.lblCaracteristicas.Name = "lblCaracteristicas";
            this.lblCaracteristicas.Size = new System.Drawing.Size(105, 17);
            this.lblCaracteristicas.TabIndex = 20;
            this.lblCaracteristicas.Text = "Caracteristicas:";
            this.lblCaracteristicas.Visible = false;
            // 
            // lblDireccion
            // 
            this.lblDireccion.AutoSize = true;
            this.lblDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDireccion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDireccion.Location = new System.Drawing.Point(736, 103);
            this.lblDireccion.Name = "lblDireccion";
            this.lblDireccion.Size = new System.Drawing.Size(71, 17);
            this.lblDireccion.TabIndex = 19;
            this.lblDireccion.Text = "Dirección:";
            this.lblDireccion.Visible = false;
            // 
            // txtDormitorios
            // 
            this.txtDormitorios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDormitorios.Location = new System.Drawing.Point(841, 187);
            this.txtDormitorios.Name = "txtDormitorios";
            this.txtDormitorios.Size = new System.Drawing.Size(244, 23);
            this.txtDormitorios.TabIndex = 18;
            this.txtDormitorios.Visible = false;
            // 
            // txtValorNoche
            // 
            this.txtValorNoche.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorNoche.Location = new System.Drawing.Point(841, 158);
            this.txtValorNoche.Name = "txtValorNoche";
            this.txtValorNoche.Size = new System.Drawing.Size(244, 23);
            this.txtValorNoche.TabIndex = 17;
            this.txtValorNoche.Visible = false;
            // 
            // txtCaracteristicas
            // 
            this.txtCaracteristicas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCaracteristicas.Location = new System.Drawing.Point(841, 129);
            this.txtCaracteristicas.Name = "txtCaracteristicas";
            this.txtCaracteristicas.Size = new System.Drawing.Size(244, 23);
            this.txtCaracteristicas.TabIndex = 16;
            this.txtCaracteristicas.Visible = false;
            // 
            // txtDireccion
            // 
            this.txtDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccion.Location = new System.Drawing.Point(841, 100);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(244, 23);
            this.txtDireccion.TabIndex = 15;
            this.txtDireccion.Visible = false;
            // 
            // dgvDepartamentos
            // 
            this.dgvDepartamentos.AllowUserToAddRows = false;
            this.dgvDepartamentos.AllowUserToDeleteRows = false;
            this.dgvDepartamentos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDepartamentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDepartamentos.Location = new System.Drawing.Point(26, 100);
            this.dgvDepartamentos.Name = "dgvDepartamentos";
            this.dgvDepartamentos.ReadOnly = true;
            this.dgvDepartamentos.RowHeadersWidth = 62;
            this.dgvDepartamentos.Size = new System.Drawing.Size(1059, 445);
            this.dgvDepartamentos.TabIndex = 27;
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblRegion.Location = new System.Drawing.Point(196, 66);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(57, 17);
            this.lblRegion.TabIndex = 28;
            this.lblRegion.Text = "Region:";
            // 
            // lblComuna
            // 
            this.lblComuna.AutoSize = true;
            this.lblComuna.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComuna.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblComuna.Location = new System.Drawing.Point(688, 66);
            this.lblComuna.Name = "lblComuna";
            this.lblComuna.Size = new System.Drawing.Size(64, 17);
            this.lblComuna.TabIndex = 29;
            this.lblComuna.Text = "Comuna:";
            // 
            // lblProvincia
            // 
            this.lblProvincia.AutoSize = true;
            this.lblProvincia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProvincia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblProvincia.Location = new System.Drawing.Point(442, 66);
            this.lblProvincia.Name = "lblProvincia";
            this.lblProvincia.Size = new System.Drawing.Size(70, 17);
            this.lblProvincia.TabIndex = 30;
            this.lblProvincia.Text = "Provincia:";
            // 
            // cbRegion
            // 
            this.cbRegion.FormattingEnabled = true;
            this.cbRegion.Location = new System.Drawing.Point(248, 65);
            this.cbRegion.Name = "cbRegion";
            this.cbRegion.Size = new System.Drawing.Size(188, 21);
            this.cbRegion.TabIndex = 31;
            this.cbRegion.SelectedIndexChanged += new System.EventHandler(this.cbRegion_SelectedIndexChanged);
            // 
            // cbComuna
            // 
            this.cbComuna.FormattingEnabled = true;
            this.cbComuna.Location = new System.Drawing.Point(749, 65);
            this.cbComuna.Name = "cbComuna";
            this.cbComuna.Size = new System.Drawing.Size(153, 21);
            this.cbComuna.TabIndex = 32;
            this.cbComuna.SelectedIndexChanged += new System.EventHandler(this.cbComuna_SelectedIndexChanged);
            // 
            // cbProvincia
            // 
            this.cbProvincia.FormattingEnabled = true;
            this.cbProvincia.Location = new System.Drawing.Point(507, 65);
            this.cbProvincia.Name = "cbProvincia";
            this.cbProvincia.Size = new System.Drawing.Size(166, 21);
            this.cbProvincia.TabIndex = 33;
            this.cbProvincia.SelectedIndexChanged += new System.EventHandler(this.cbProvincia_SelectedIndexChanged);
            // 
            // lblCamas
            // 
            this.lblCamas.AutoSize = true;
            this.lblCamas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCamas.Location = new System.Drawing.Point(736, 220);
            this.lblCamas.Name = "lblCamas";
            this.lblCamas.Size = new System.Drawing.Size(55, 17);
            this.lblCamas.TabIndex = 34;
            this.lblCamas.Text = "Camas:";
            this.lblCamas.Visible = false;
            // 
            // txtCamas
            // 
            this.txtCamas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCamas.Location = new System.Drawing.Point(841, 214);
            this.txtCamas.Name = "txtCamas";
            this.txtCamas.Size = new System.Drawing.Size(244, 23);
            this.txtCamas.TabIndex = 35;
            this.txtCamas.Visible = false;
            // 
            // btnAgregar
            // 
            this.btnAgregar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnAgregar.FlatAppearance.BorderSize = 0;
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregar.ForeColor = System.Drawing.Color.White;
            this.btnAgregar.Location = new System.Drawing.Point(841, 252);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(100, 35);
            this.btnAgregar.TabIndex = 40;
            this.btnAgregar.Text = "Crear";
            this.btnAgregar.UseVisualStyleBackColor = false;
            this.btnAgregar.Visible = false;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // lblDepartamentos
            // 
            this.lblDepartamentos.AutoSize = true;
            this.lblDepartamentos.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartamentos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDepartamentos.Location = new System.Drawing.Point(474, 20);
            this.lblDepartamentos.Name = "lblDepartamentos";
            this.lblDepartamentos.Size = new System.Drawing.Size(177, 29);
            this.lblDepartamentos.TabIndex = 41;
            this.lblDepartamentos.Text = "Departamentos";
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.White;
            this.btnCancelar.Location = new System.Drawing.Point(985, 252);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(100, 35);
            this.btnCancelar.TabIndex = 42;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblCrearDepartamento
            // 
            this.lblCrearDepartamento.AutoSize = true;
            this.lblCrearDepartamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCrearDepartamento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCrearDepartamento.Location = new System.Drawing.Point(845, 53);
            this.lblCrearDepartamento.Name = "lblCrearDepartamento";
            this.lblCrearDepartamento.Size = new System.Drawing.Size(231, 29);
            this.lblCrearDepartamento.TabIndex = 43;
            this.lblCrearDepartamento.Text = "Crear Departamento";
            this.lblCrearDepartamento.Visible = false;
            // 
            // btnAgregarDepartamentos
            // 
            this.btnAgregarDepartamentos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnAgregarDepartamentos.FlatAppearance.BorderSize = 0;
            this.btnAgregarDepartamentos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarDepartamentos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarDepartamentos.ForeColor = System.Drawing.Color.White;
            this.btnAgregarDepartamentos.Location = new System.Drawing.Point(931, 53);
            this.btnAgregarDepartamentos.Name = "btnAgregarDepartamentos";
            this.btnAgregarDepartamentos.Size = new System.Drawing.Size(154, 35);
            this.btnAgregarDepartamentos.TabIndex = 44;
            this.btnAgregarDepartamentos.Text = "Agregar Departamentos";
            this.btnAgregarDepartamentos.UseVisualStyleBackColor = false;
            this.btnAgregarDepartamentos.Click += new System.EventHandler(this.btnAgregarDepartamentos_Click);
            // 
            // btnEditarDepartamento
            // 
            this.btnEditarDepartamento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnEditarDepartamento.FlatAppearance.BorderSize = 0;
            this.btnEditarDepartamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarDepartamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarDepartamento.ForeColor = System.Drawing.Color.White;
            this.btnEditarDepartamento.Location = new System.Drawing.Point(336, 559);
            this.btnEditarDepartamento.Name = "btnEditarDepartamento";
            this.btnEditarDepartamento.Size = new System.Drawing.Size(100, 35);
            this.btnEditarDepartamento.TabIndex = 45;
            this.btnEditarDepartamento.Text = "Editar";
            this.btnEditarDepartamento.UseVisualStyleBackColor = false;
            this.btnEditarDepartamento.Click += new System.EventHandler(this.btnEditarDepartamento_Click);
            // 
            // Departamentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 606);
            this.Controls.Add(this.btnEditarDepartamento);
            this.Controls.Add(this.btnAgregarDepartamentos);
            this.Controls.Add(this.lblCrearDepartamento);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblDepartamentos);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.txtCamas);
            this.Controls.Add(this.lblCamas);
            this.Controls.Add(this.cbProvincia);
            this.Controls.Add(this.cbComuna);
            this.Controls.Add(this.cbRegion);
            this.Controls.Add(this.lblProvincia);
            this.Controls.Add(this.lblComuna);
            this.Controls.Add(this.lblRegion);
            this.Controls.Add(this.dgvDepartamentos);
            this.Controls.Add(this.lblDormitorios);
            this.Controls.Add(this.lblValorNoche);
            this.Controls.Add(this.lblCaracteristicas);
            this.Controls.Add(this.lblDireccion);
            this.Controls.Add(this.txtDormitorios);
            this.Controls.Add(this.txtValorNoche);
            this.Controls.Add(this.txtCaracteristicas);
            this.Controls.Add(this.txtDireccion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Departamentos";
            this.Text = "Departamentos";
            this.Load += new System.EventHandler(this.Departamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDepartamentos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblDormitorios;
        private System.Windows.Forms.Label lblValorNoche;
        private System.Windows.Forms.Label lblCaracteristicas;
        private System.Windows.Forms.Label lblDireccion;
        public System.Windows.Forms.TextBox txtDormitorios;
        public System.Windows.Forms.TextBox txtValorNoche;
        public System.Windows.Forms.TextBox txtCaracteristicas;
        public System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.DataGridView dgvDepartamentos;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.Label lblComuna;
        private System.Windows.Forms.Label lblProvincia;
        private System.Windows.Forms.ComboBox cbRegion;
        private System.Windows.Forms.ComboBox cbComuna;
        private System.Windows.Forms.ComboBox cbProvincia;
        private System.Windows.Forms.Label lblCamas;
        public System.Windows.Forms.TextBox txtCamas;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Label lblDepartamentos;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label lblCrearDepartamento;
        private System.Windows.Forms.Button btnAgregarDepartamentos;
        private System.Windows.Forms.Button btnEditarDepartamento;
    }
}
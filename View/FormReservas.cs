﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using Controller;

namespace View
{
    public partial class FormReservas : Form
    {
        public FormReservas()
        {
            InitializeComponent();
        }

        private int idUsuario;
        private int idDepartamento;

        public async Task cargarRegionesAsync()
        {
            if (await RegionController.listarRegiones() != null)
            {
                cbRegion.DisplayMember = "nombre";
                cbRegion.ValueMember = "id";
                cbRegion.DataSource = await RegionController.listarRegiones();
            }

        }

        public async Task listarChoferes(int idEmpresa)
        {
            if (await ChoferController.listarChoferesPorEmpresa(idEmpresa) != null)
            {
                cbChoferes.DisplayMember = "nombre";
                cbChoferes.ValueMember = "id";
                cbChoferes.DataSource = await ChoferController.listarChoferesPorEmpresa(idEmpresa);
            }

        }

        private void calcularTotal()
        {
            try
            {
                int valorTour = 0;
                int valorTotalConTour = 0;
                int total = 0;
                int valorNoche = 0;
                int personas = int.Parse(txtPersonas.Text);
                int cantidadDias = Convert.ToInt32((dtpFechaTermino.Value - dtpFechaInicio.Value).TotalDays);

                Departamento departamento = cbDepartamentos.SelectedItem as Departamento;
                valorNoche = departamento.ValorNoche;
                idDepartamento = departamento.Id;

                total = cantidadDias * valorNoche * personas;

                if (chbTours.Checked)
                {
                    if (dgvTours.SelectedRows.Count > 0)
                    {

                        valorTour = int.Parse(dgvTours.CurrentRow.Cells["VALOR PERSONA"].Value.ToString());
                        valorTotalConTour = personas * valorTour;
                        total = total + valorTotalConTour;
                    }
                    else
                    {
                        MessageBox.Show("Selecione un tour");
                    }
                }
                txtTotal.Text = total.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor verificar que ningun campo este vacio y con caracteres no correspondientes");
            }

        }

        private async void listarTours(int idComuna)
        {

            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("DESCRIPCION");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("VALOR PERSONA");
            dataTable.Columns.Add(dataColumn);

            if (await TourController.listarTours(idComuna) != null)
            {
                foreach (Tour tours in await TourController.listarTours(idComuna))
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = tours.Id;
                    dataRow[1] = tours.Nombre;
                    dataRow[2] = tours.Descripcion;
                    dataRow[3] = tours.Valor_Persona;

                    dataTable.Rows.Add(dataRow);

                }

                dgvTours.DataSource = dataTable;
                dgvTours.Columns[0].Visible = false;
            }
        }

        private async void listarEmpresas()
        {

            if (await EmpresaController.listarEmpresas() != null)
            {
                cbEmpresa.DisplayMember = "nombre";
                cbEmpresa.ValueMember = "id";
                cbEmpresa.DataSource = await EmpresaController.listarEmpresas();
            }
        }

        private async void ListarUsuariosAsync()
        {
            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE USUARIO");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("CORREO");
            dataTable.Columns.Add(dataColumn);


            if (await UsuarioController.listarUsuarios() != null)
            {
                foreach (Usuario usuario in await UsuarioController.listarUsuarios())
                {
                    foreach (Perfil perfil in usuario.Perfil)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow[0] = usuario.Id;
                        dataRow[1] = usuario.Nombre;
                        dataRow[2] = usuario.Username;
                        dataRow[3] = usuario.Correo;

                        dataTable.Rows.Add(dataRow);
                    }
                }

                dgvUsuarios.DataSource = dataTable;
                dgvUsuarios.Columns[0].Visible = false;
            }

        }

        private async void listarDepartamentos(int idComuna)
        {
            if (await DepartamentoController.listarDepartamentos(idComuna) != null)
            {

                cbDepartamentos.DataSource = await DepartamentoController.listarDepartamentos(idComuna);
                cbDepartamentos.DisplayMember = "direccion";

            }
        }


        private void chkTraslado_Click(object sender, EventArgs e)
        {
            try
            {

                if (chkTraslado.Checked)
                {
                    lblSeleccionarTraslado.Visible = true;
                    lblTrasIni.Visible = true;
                    dpTrasladoFechaIni.Visible = true;
                    lblDirRetiro.Visible = true;
                    txtDirRetiro.Visible = true;
                    lblEmpresa.Visible = true;
                    cbEmpresa.Visible = true;
                    lblChoferes.Visible = true;
                    cbChoferes.Visible = true;
                    listarEmpresas();

                }
                else
                {
                    lblSeleccionarTraslado.Visible = false;
                    lblTrasIni.Visible = false;
                    dpTrasladoFechaIni.Visible = false;
                    lblDirRetiro.Visible = false;
                    txtDirRetiro.Visible = false;
                    lblEmpresa.Visible = false;
                    cbEmpresa.Visible = false;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Espere a que cargue el sistema");
            }
        }

        private async void cbRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (await ProvinciaController.listarProvincias(int.Parse(cbRegion.SelectedValue.ToString())) != null)
            {
                cbProvincia.DisplayMember = "nombre";
                cbProvincia.ValueMember = "id";
                cbProvincia.DataSource = await ProvinciaController.listarProvincias(int.Parse(cbRegion.SelectedValue.ToString()));
            }
        }

        private async void cbProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (await ComunaController.listarComunas(int.Parse(cbProvincia.SelectedValue.ToString())) != null)
            {
                cbComuna.DisplayMember = "nombre";
                cbComuna.ValueMember = "id";
                cbComuna.DataSource = await ComunaController.listarComunas(int.Parse(cbProvincia.SelectedValue.ToString()));
            }
        }

        private async void cbComuna_SelectedIndexChanged(object sender, EventArgs e)
        {
            listarDepartamentos(int.Parse(cbComuna.SelectedValue.ToString()));
        }

        private async void FormReservas_Load(object sender, EventArgs e)
        {
            await cargarRegionesAsync();
            ListarUsuariosAsync();
            txtTotal.Text = "0";

        }

        private void chbTours_Click(object sender, EventArgs e)
        {
            try
            {

                if (chbTours.Checked)
                {
                    lblBuscar.Visible = true;
                    txtBuscar.Visible = true;
                    lblSeleccionarTour.Visible = true;
                    dgvTours.Visible = true;
                    chbConfirmarReserva.Visible = true;
                    listarTours(int.Parse(cbComuna.SelectedValue.ToString()));
                }
                else
                {
                    lblBuscar.Visible = false;
                    txtBuscar.Visible = false;
                    lblSeleccionarTour.Visible = false;
                    dgvTours.Visible = false;
                    chbConfirmarReserva.Visible = false;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Espere a que cargue el sistema");
            }
        }



        private void txtBuscador_KeyPress(object sender, KeyPressEventArgs e)
        {
            (dgvUsuarios.DataSource as DataTable).DefaultView.RowFilter = string.Format("NOMBRE LIKE'{0}%'", txtBuscador.Text);
        }

        private async void cbEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            await listarChoferes(int.Parse(cbEmpresa.SelectedValue.ToString()));
        }

        private void txtPersonas_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void btnTotal_Click(object sender, EventArgs e)
        {
            calcularTotal();
        }

        private async void btnRealizarReserva_Click(object sender, EventArgs e)
        {
            try
            {

                if (dgvUsuarios.SelectedRows.Count > 0)
                {
                    int idReserva;
                    idUsuario = int.Parse(dgvUsuarios.CurrentRow.Cells["ID"].Value.ToString());

                    idReserva = await ReservaController.insertarReserva(dtpFechaInicio.Value.ToString("dd-MM-yyyy"), dtpFechaTermino.Value.ToString("dd-MM-yyyy"), int.Parse(txtPersonas.Text), idUsuario, idDepartamento, int.Parse(txtTotal.Text));
                    if (idReserva != 0)
                    {

                        if (chkTraslado.Checked)
                        {
                            await TrasladoController.crearTraslado(dpTrasladoFechaIni.Value.ToString("dd-MM-yyyy"), txtDirRetiro.Text, int.Parse(cbChoferes.SelectedValue.ToString()), idReserva);

                        }

                        if (chbTours.Checked)
                        {
                            if (dgvTours.SelectedRows.Count > 0)
                            {
                                int idTour = int.Parse(dgvTours.CurrentRow.Cells["ID"].Value.ToString());

                                await TourController.InsertarReservaTour(idTour,idReserva);
                            }
                            else
                            {
                                MessageBox.Show("Debe seleccionar un tour");
                            }

                        }

                        MessageBox.Show("Reserva ingresada correctamente");
     
                    }
                    else
                    {

                        MessageBox.Show("Reserva no ingresada, intente nuevamente");
                    }
                }
                else
                {
                    MessageBox.Show("Debe seleccionar un usuario ");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor verificar que ningun campo este vacio y con caracteres no correspondientes");
            }

        }
    }
}

           
    


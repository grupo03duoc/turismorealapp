﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using Controller;
using Response;

namespace View
{
    public partial class Empresas : Form
    {
        public Empresas()
        {
            InitializeComponent();
        }
        private bool editar = false;
        private int idEmpresa;

        private async void listarEmpresas()
        {

            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("RUT");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("RUBRO");
            dataTable.Columns.Add(dataColumn);
           

            if (await EmpresaController.listarEmpresas()!= null)
            {
                foreach (Empresa empresa in await EmpresaController.listarEmpresas())
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = empresa.id;
                    dataRow[1] = empresa.nombre;
                    dataRow[2] = empresa.rut;
                    dataRow[3] = empresa.rubro;
      
                    dataTable.Rows.Add(dataRow);

                }

                dgvEmpresas.DataSource = dataTable;
                dgvEmpresas.Columns[0].Visible = false;
            }
        }

        private void Empresa_Load(object sender, EventArgs e)
        {
            listarEmpresas();
        }

       private void limpiarFormEmpresa()
        {
            txtNombre.Text = "";
            txtRubro.Text = "";
            txtRut.Text = "";
        }

        private async void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {

                if (editar == false)
                {
                    if (await EmpresaController.insertarEmpresa(txtNombre.Text,txtRubro.Text,txtRut.Text))
                    {

                        MessageBox.Show("Empresa Ingresada Correctamente");
                        listarEmpresas();
                        limpiarFormEmpresa();

                    }
                    else
                    {
                        MessageBox.Show("Empresa No ingresada, intente nuevamente");
                        listarEmpresas();
                        limpiarFormEmpresa();

                    }
                }
                else if (editar == true)
                {
                    if (await EmpresaController.EditarEmpresa(idEmpresa, txtNombre.Text, txtRubro.Text, txtRut.Text))
                    {
                        MessageBox.Show("Empresa Modificada Correctamente");
                        listarEmpresas();
                        limpiarFormEmpresa();
                    }
                    else
                    {
                        MessageBox.Show("Empresa No Modificada, intente nuevamente");
                        listarEmpresas();
                        limpiarFormEmpresa();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor verificar que ningun campo este vacio y con caracteres no correspondientes");

            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarFormEmpresa();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dgvEmpresas.SelectedRows.Count > 0)
            {
                limpiarFormEmpresa();
                lblCrearEmpresa.Text = "Editar Empresa";
                btnAgregar.Text = "Editar";
                idEmpresa = int.Parse(dgvEmpresas.CurrentRow.Cells["ID"].Value.ToString());
                editar = true;
                txtNombre.Text = dgvEmpresas.CurrentRow.Cells["NOMBRE"].Value.ToString();
                txtRut.Text = dgvEmpresas.CurrentRow.Cells["RUT"].Value.ToString();
                txtRubro.Text = dgvEmpresas.CurrentRow.Cells["RUBRO"].Value.ToString();
            }
            else
            {
                MessageBox.Show("Seleccione la fila a editar: ");
            }
        }
    }
}

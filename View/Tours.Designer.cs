﻿namespace View
{
    partial class Tours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTours = new System.Windows.Forms.Label();
            this.cbRegion = new System.Windows.Forms.ComboBox();
            this.lblRegion = new System.Windows.Forms.Label();
            this.dgvTours = new System.Windows.Forms.DataGridView();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.cbProvincia = new System.Windows.Forms.ComboBox();
            this.cbComuna = new System.Windows.Forms.ComboBox();
            this.lblProvincia = new System.Windows.Forms.Label();
            this.lblComuna = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.lblCrearTour = new System.Windows.Forms.Label();
            this.lblValorPersona = new System.Windows.Forms.Label();
            this.btnInsertarTour = new System.Windows.Forms.Button();
            this.lblDescripcion = new System.Windows.Forms.Label();
            this.lblNombreTour = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.txtNombreTour = new System.Windows.Forms.TextBox();
            this.txtValorPersona = new System.Windows.Forms.TextBox();
            this.btnCrearTour = new System.Windows.Forms.Button();
            this.btnEditarTour = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTours)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTours
            // 
            this.lblTours.AutoSize = true;
            this.lblTours.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTours.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTours.Location = new System.Drawing.Point(511, 26);
            this.lblTours.Name = "lblTours";
            this.lblTours.Size = new System.Drawing.Size(76, 29);
            this.lblTours.TabIndex = 64;
            this.lblTours.Text = "Tours";
            // 
            // cbRegion
            // 
            this.cbRegion.FormattingEnabled = true;
            this.cbRegion.Location = new System.Drawing.Point(212, 68);
            this.cbRegion.Name = "cbRegion";
            this.cbRegion.Size = new System.Drawing.Size(188, 21);
            this.cbRegion.TabIndex = 80;
            this.cbRegion.SelectedIndexChanged += new System.EventHandler(this.cbRegion_SelectedIndexChanged);
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblRegion.Location = new System.Drawing.Point(158, 69);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(57, 17);
            this.lblRegion.TabIndex = 79;
            this.lblRegion.Text = "Region:";
            // 
            // dgvTours
            // 
            this.dgvTours.AllowUserToAddRows = false;
            this.dgvTours.AllowUserToDeleteRows = false;
            this.dgvTours.AllowUserToOrderColumns = true;
            this.dgvTours.AllowUserToResizeRows = false;
            this.dgvTours.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTours.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTours.Location = new System.Drawing.Point(25, 132);
            this.dgvTours.Name = "dgvTours";
            this.dgvTours.ReadOnly = true;
            this.dgvTours.Size = new System.Drawing.Size(1054, 385);
            this.dgvTours.TabIndex = 81;
            // 
            // txtBuscar
            // 
            this.txtBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscar.Location = new System.Drawing.Point(435, 103);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(244, 23);
            this.txtBuscar.TabIndex = 84;
            this.txtBuscar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBuscar_KeyPress);
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblBuscar.Location = new System.Drawing.Point(373, 106);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(56, 17);
            this.lblBuscar.TabIndex = 83;
            this.lblBuscar.Text = "Buscar:";
            // 
            // cbProvincia
            // 
            this.cbProvincia.FormattingEnabled = true;
            this.cbProvincia.Location = new System.Drawing.Point(471, 68);
            this.cbProvincia.Name = "cbProvincia";
            this.cbProvincia.Size = new System.Drawing.Size(166, 21);
            this.cbProvincia.TabIndex = 88;
            this.cbProvincia.SelectedIndexChanged += new System.EventHandler(this.cbProvincia_SelectedIndexChanged);
            // 
            // cbComuna
            // 
            this.cbComuna.FormattingEnabled = true;
            this.cbComuna.Location = new System.Drawing.Point(713, 68);
            this.cbComuna.Name = "cbComuna";
            this.cbComuna.Size = new System.Drawing.Size(153, 21);
            this.cbComuna.TabIndex = 87;
            this.cbComuna.SelectedIndexChanged += new System.EventHandler(this.cbComuna_SelectedIndexChanged);
            // 
            // lblProvincia
            // 
            this.lblProvincia.AutoSize = true;
            this.lblProvincia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProvincia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblProvincia.Location = new System.Drawing.Point(406, 69);
            this.lblProvincia.Name = "lblProvincia";
            this.lblProvincia.Size = new System.Drawing.Size(70, 17);
            this.lblProvincia.TabIndex = 86;
            this.lblProvincia.Text = "Provincia:";
            // 
            // lblComuna
            // 
            this.lblComuna.AutoSize = true;
            this.lblComuna.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComuna.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblComuna.Location = new System.Drawing.Point(652, 69);
            this.lblComuna.Name = "lblComuna";
            this.lblComuna.Size = new System.Drawing.Size(64, 17);
            this.lblComuna.TabIndex = 85;
            this.lblComuna.Text = "Comuna:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.White;
            this.btnCancelar.Location = new System.Drawing.Point(980, 232);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(99, 35);
            this.btnCancelar.TabIndex = 105;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Visible = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblCrearTour
            // 
            this.lblCrearTour.AutoSize = true;
            this.lblCrearTour.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCrearTour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCrearTour.Location = new System.Drawing.Point(872, 80);
            this.lblCrearTour.Name = "lblCrearTour";
            this.lblCrearTour.Size = new System.Drawing.Size(130, 29);
            this.lblCrearTour.TabIndex = 104;
            this.lblCrearTour.Text = "Crear Tour";
            this.lblCrearTour.Visible = false;
            // 
            // lblValorPersona
            // 
            this.lblValorPersona.AutoSize = true;
            this.lblValorPersona.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorPersona.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblValorPersona.Location = new System.Drawing.Point(744, 192);
            this.lblValorPersona.Name = "lblValorPersona";
            this.lblValorPersona.Size = new System.Drawing.Size(45, 17);
            this.lblValorPersona.TabIndex = 101;
            this.lblValorPersona.Text = "Valor:";
            this.lblValorPersona.Visible = false;
            // 
            // btnInsertarTour
            // 
            this.btnInsertarTour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnInsertarTour.FlatAppearance.BorderSize = 0;
            this.btnInsertarTour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInsertarTour.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsertarTour.ForeColor = System.Drawing.Color.White;
            this.btnInsertarTour.Location = new System.Drawing.Point(835, 232);
            this.btnInsertarTour.Name = "btnInsertarTour";
            this.btnInsertarTour.Size = new System.Drawing.Size(99, 35);
            this.btnInsertarTour.TabIndex = 97;
            this.btnInsertarTour.Text = "Crear";
            this.btnInsertarTour.UseVisualStyleBackColor = false;
            this.btnInsertarTour.Visible = false;
            this.btnInsertarTour.Click += new System.EventHandler(this.btnInsertarTour_Click);
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.AutoSize = true;
            this.lblDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDescripcion.Location = new System.Drawing.Point(744, 162);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(86, 17);
            this.lblDescripcion.TabIndex = 94;
            this.lblDescripcion.Text = "Descripcion:";
            this.lblDescripcion.Visible = false;
            // 
            // lblNombreTour
            // 
            this.lblNombreTour.AutoSize = true;
            this.lblNombreTour.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreTour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNombreTour.Location = new System.Drawing.Point(743, 133);
            this.lblNombreTour.Name = "lblNombreTour";
            this.lblNombreTour.Size = new System.Drawing.Size(62, 17);
            this.lblNombreTour.TabIndex = 93;
            this.lblNombreTour.Text = "Nombre:";
            this.lblNombreTour.Visible = false;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.Location = new System.Drawing.Point(836, 159);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(244, 23);
            this.txtDescripcion.TabIndex = 90;
            this.txtDescripcion.Visible = false;
            // 
            // txtNombreTour
            // 
            this.txtNombreTour.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreTour.Location = new System.Drawing.Point(836, 130);
            this.txtNombreTour.Name = "txtNombreTour";
            this.txtNombreTour.Size = new System.Drawing.Size(244, 23);
            this.txtNombreTour.TabIndex = 89;
            this.txtNombreTour.Visible = false;
            // 
            // txtValorPersona
            // 
            this.txtValorPersona.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValorPersona.Location = new System.Drawing.Point(835, 189);
            this.txtValorPersona.Name = "txtValorPersona";
            this.txtValorPersona.Size = new System.Drawing.Size(244, 23);
            this.txtValorPersona.TabIndex = 106;
            this.txtValorPersona.Visible = false;
            // 
            // btnCrearTour
            // 
            this.btnCrearTour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnCrearTour.FlatAppearance.BorderSize = 0;
            this.btnCrearTour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCrearTour.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrearTour.ForeColor = System.Drawing.Color.White;
            this.btnCrearTour.Location = new System.Drawing.Point(980, 88);
            this.btnCrearTour.Name = "btnCrearTour";
            this.btnCrearTour.Size = new System.Drawing.Size(100, 35);
            this.btnCrearTour.TabIndex = 107;
            this.btnCrearTour.Text = "Crear Tour";
            this.btnCrearTour.UseVisualStyleBackColor = false;
            this.btnCrearTour.Click += new System.EventHandler(this.btnCrearTour_Click);
            // 
            // btnEditarTour
            // 
            this.btnEditarTour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnEditarTour.FlatAppearance.BorderSize = 0;
            this.btnEditarTour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditarTour.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditarTour.ForeColor = System.Drawing.Color.White;
            this.btnEditarTour.Location = new System.Drawing.Point(506, 537);
            this.btnEditarTour.Name = "btnEditarTour";
            this.btnEditarTour.Size = new System.Drawing.Size(100, 35);
            this.btnEditarTour.TabIndex = 108;
            this.btnEditarTour.Text = "Editar";
            this.btnEditarTour.UseVisualStyleBackColor = false;
            this.btnEditarTour.Click += new System.EventHandler(this.btnEditarTour_Click);
            // 
            // Tours
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 606);
            this.Controls.Add(this.btnEditarTour);
            this.Controls.Add(this.btnCrearTour);
            this.Controls.Add(this.txtValorPersona);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblCrearTour);
            this.Controls.Add(this.lblValorPersona);
            this.Controls.Add(this.btnInsertarTour);
            this.Controls.Add(this.lblDescripcion);
            this.Controls.Add(this.lblNombreTour);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.txtNombreTour);
            this.Controls.Add(this.cbProvincia);
            this.Controls.Add(this.cbComuna);
            this.Controls.Add(this.lblProvincia);
            this.Controls.Add(this.lblComuna);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.lblBuscar);
            this.Controls.Add(this.dgvTours);
            this.Controls.Add(this.cbRegion);
            this.Controls.Add(this.lblRegion);
            this.Controls.Add(this.lblTours);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Tours";
            this.Text = "Tours";
            this.Load += new System.EventHandler(this.Tours_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTours;
        private System.Windows.Forms.ComboBox cbRegion;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.DataGridView dgvTours;
        public System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.ComboBox cbProvincia;
        private System.Windows.Forms.ComboBox cbComuna;
        private System.Windows.Forms.Label lblProvincia;
        private System.Windows.Forms.Label lblComuna;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label lblCrearTour;
        private System.Windows.Forms.Label lblValorPersona;
        private System.Windows.Forms.Button btnInsertarTour;
        private System.Windows.Forms.Label lblDescripcion;
        private System.Windows.Forms.Label lblNombreTour;
        public System.Windows.Forms.TextBox txtDescripcion;
        public System.Windows.Forms.TextBox txtNombreTour;
        public System.Windows.Forms.TextBox txtValorPersona;
        private System.Windows.Forms.Button btnCrearTour;
        private System.Windows.Forms.Button btnEditarTour;
    }
}
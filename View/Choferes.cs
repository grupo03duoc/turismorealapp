﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controller;
using Model;

namespace View
{
    public partial class Choferes : Form
    {
        public Choferes()
        {
            InitializeComponent();
        }

        private bool editar = false;
        private int idEmpresa;
        private int idChofer;

        private async void listarEmpresas()
        {

            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("RUT");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("RUBRO");
            dataTable.Columns.Add(dataColumn);


            if (await EmpresaController.listarEmpresas() != null)
            {
                foreach (Empresa empresa in await EmpresaController.listarEmpresas())
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = empresa.id;
                    dataRow[1] = empresa.nombre;
                    dataRow[2] = empresa.rut;
                    dataRow[3] = empresa.rubro;

                    dataTable.Rows.Add(dataRow);

                }

                dgvEmpresa.DataSource = dataTable;
                dgvEmpresa.Columns[0].Visible = false;
            }
        }

        private async void listarChoferes(int idEmpresa)
        {

            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("RUT");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("VEHICULO");
            dataTable.Columns.Add(dataColumn);


            if (await ChoferController.listarChoferesPorEmpresa(idEmpresa) != null)
            {
                foreach (Chofer chofer in await ChoferController.listarChoferesPorEmpresa(idEmpresa))
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = chofer.id;
                    dataRow[1] = chofer.nombre;
                    dataRow[2] = chofer.rut;
                    dataRow[3] = chofer.vehiculo;

                    dataTable.Rows.Add(dataRow);

                }

                dgvChoferes.DataSource = dataTable;
                dgvChoferes.Columns[0].Visible = false;
            }
        }

        private void limpiarFormChofer()
        {
            lblCrearChofer.Text = "Crear Chofer";
            txtNombre.Text = "";
            txtRut.Text = "";
            txtVehiculo.Text = "";
            btnAgregar.Text = "Crear";

        }

        private async void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {

                if (editar == false)
                {
                    if (await ChoferController.insertarChofer(txtNombre.Text, txtRut.Text, txtVehiculo.Text, idEmpresa))
                    {

                        MessageBox.Show("Chofer Ingresado Correctamente");
                        listarEmpresas();
                        listarChoferes(idEmpresa);
                        limpiarFormChofer();

                    }
                    else
                    {
                        MessageBox.Show("Chofer No ingresado, intente nuevamente");
                        listarEmpresas();
                        listarChoferes(idEmpresa);
                        limpiarFormChofer();

                    }
                }
                else if (editar == true)
                {
                    if (await ChoferController.EditarChofer(idChofer, txtNombre.Text, txtRut.Text, txtVehiculo.Text, idEmpresa))
                    {
                        MessageBox.Show("Chofer Modificado Correctamente");
                        listarEmpresas();
                        listarChoferes(idEmpresa);
                        limpiarFormChofer();

                    }
                    else
                    {
                        MessageBox.Show("Chofer No Modificado, intente nuevamente");
                        listarEmpresas();
                        listarChoferes(idEmpresa);
                        limpiarFormChofer();

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor verificar que ningun campo este vacio y con caracteres no correspondientes");

            }
        }

        private void Choferes_Load(object sender, EventArgs e)
        {
            listarEmpresas();
        }

        private void dgvEmpresa_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvEmpresa.SelectedRows.Count > 0)
            {
                 idEmpresa = int.Parse(dgvEmpresa.CurrentRow.Cells["ID"].Value.ToString());
                listarChoferes(idEmpresa);

            }
            else
            {
                MessageBox.Show("Seleccione una empresa para ver los choferes ");
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dgvChoferes.SelectedRows.Count > 0)
            {
                idChofer = int.Parse(dgvChoferes.CurrentRow.Cells["ID"].Value.ToString());
                lblCrearChofer.Text = "Editar Chofer";
                btnAgregar.Text = "Editar";
                editar = true;
                txtNombre.Text = dgvChoferes.CurrentRow.Cells["NOMBRE"].Value.ToString();
                txtRut.Text = dgvChoferes.CurrentRow.Cells["RUT"].Value.ToString();
                txtVehiculo.Text = dgvChoferes.CurrentRow.Cells["VEHICULO"].Value.ToString();

            }
            else
            {
                MessageBox.Show("Seleccione un chofer a editar: ");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarFormChofer();
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Controller;
using Model;

namespace View
{
    public partial class Usuarios : Form
    {
        //Variables que se usaran para determinar que metodos usar, ademas de almacenar la id del usuario seleccionado.
        private bool editar = false;
        private int idUsuario;

        public Usuarios()
        {
            InitializeComponent();
        }

        //Se deja por defecto al abrir el form Usuarios que se listen los Usuarios, ademas de agregar datos a los combobox perfil y estado usuario.
        private void Usuarios_Load(object sender, EventArgs e)
        {
            ListarUsuariosAsync();
            cbPerfilUsuario.Items.Add("Usuario");
            cbPerfilUsuario.Items.Add("Administrador");
            cbEstadoUsuario.Items.Add("Habilitado");
            cbEstadoUsuario.Items.Add("Deshabilitado");
        }

        //Se establece que la fuente de datos de la grilla sera el listar Usuarios, ademas de personalizar y cargar los datos a la grilla.
        private async void ListarUsuariosAsync()
        {
            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE USUARIO");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("PERFIL");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("CORREO");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("ESTADO");
            dataTable.Columns.Add(dataColumn);


            if (await UsuarioController.listarUsuarios() != null)
            {
                foreach (Usuario usuario in await UsuarioController.listarUsuarios())
                {
                    foreach (Perfil perfil in usuario.Perfil)
                    {
                        DataRow dataRow = dataTable.NewRow();
                        dataRow[0] = usuario.Id;
                        dataRow[1] = usuario.Nombre;
                        dataRow[2] = usuario.Username;
                        if (perfil.Id == 1)
                        {
                            dataRow[3] = "Usuario";
                        }
                        else
                        {
                            dataRow[3] = "Administrador";
                        }
                        dataRow[4] = usuario.Correo;
                        if (usuario.estadoUsuario == true)
                        {
                            dataRow[5] = "Habilitado";
                        }
                        else
                        {
                            dataRow[5] = "Deshabilitado";
                        }


                        dataTable.Rows.Add(dataRow);
                    }
                }

                dgvUsuarios.DataSource = dataTable;
                dgvUsuarios.Columns[0].Visible = false;
            }

        }

        //Metodo que permite insertar o editar un usuario dependiendo del boton seleccionado.
        private async void btnGuardarUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                int estadoUsuario = 0;
                if (editar == false)
                {

                    if (txtContraseñaUsuario.Text == txtConfirmar.Text)
                    {

                        if (cbEstadoUsuario.SelectedItem.ToString() == "Habilitado")
                        {
                            estadoUsuario = 1;
                        }

                        if (await UsuarioController.insertarUsuario(txtNombreUsuario.Text, txtUsernameUsuario.Text, txtCorreo.Text, cbPerfilUsuario.SelectedItem.ToString(), txtContraseñaUsuario.Text, estadoUsuario))
                        {
                            MessageBox.Show("Usuario insertado Correctamente");
                            ListarUsuariosAsync();
                            limpiarFormUsuario();
                        }
                        else
                        {
                            MessageBox.Show("No se pudo insertar el usuario, intente nuevamente");
                            ListarUsuariosAsync();
                            limpiarFormUsuario();
                        }
                       
                    }
                    else
                    {
                        MessageBox.Show("Las contraseñas no coinciden");
                    }
                }
                else if (editar == true)
                {

                    if (cbEstadoUsuario.SelectedItem.ToString() == "Habilitado")
                    {
                        estadoUsuario = 1;
                    }

                    if (await UsuarioController.EditarUsuario(idUsuario, txtNombreUsuario.Text, txtUsernameUsuario.Text, txtCorreo.Text, cbPerfilUsuario.SelectedItem.ToString(), estadoUsuario))
                    {
                        MessageBox.Show("Usuario Editado Correctamente");
                        ListarUsuariosAsync();
                        limpiarFormUsuario();

                    }else {
                        MessageBox.Show("No se pudo Editar el usuario, intente nuevamente");
                        ListarUsuariosAsync();
                        limpiarFormUsuario();
                    }


                    editar = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor verificar que ningun campo este vacio y con caracteres no correspondientes");

            }

        }



        //Metodo que se encarga de cargar la infomacion de la fila seleccionada en los textbox y combobox al presionar el boton Editar.
        private void btnEditarUsuario_Click(object sender, EventArgs e)
        {
            if (dgvUsuarios.SelectedRows.Count > 0)
            {
                menuCrearEditar();
                idUsuario = int.Parse(dgvUsuarios.CurrentRow.Cells["ID"].Value.ToString());
                btnGuardarUsuario.Text = "Modificar";
                txtContraseñaUsuario.Enabled = false;
                txtConfirmar.Enabled = false;
                editar = true;
                txtNombreUsuario.Text = dgvUsuarios.CurrentRow.Cells["NOMBRE"].Value.ToString();
                txtUsernameUsuario.Text = dgvUsuarios.CurrentRow.Cells["NOMBRE USUARIO"].Value.ToString();
                txtCorreo.Text = dgvUsuarios.CurrentRow.Cells["CORREO"].Value.ToString();
                cbEstadoUsuario.Text = dgvUsuarios.CurrentRow.Cells["ESTADO"].Value.ToString();
                cbPerfilUsuario.Text = dgvUsuarios.CurrentRow.Cells["PERFIL"].Value.ToString();

            }
            else
            {
                MessageBox.Show("Seleccione la fila a editar: ");
            }

        }

        //Metodo que permite limpiar todos los combobox y textbox.
        private void limpiarFormUsuario()
        {
            txtUsernameUsuario.Clear();
            txtContraseñaUsuario.Clear();
            cbEstadoUsuario.Text = "";
            txtNombreUsuario.Clear();
            cbPerfilUsuario.Text = "";
            txtCorreo.Clear();
            txtConfirmar.Clear();
            txtContraseñaUsuario.Enabled = true;
            txtConfirmar.Enabled = true;
            btnGuardarUsuario.Text = "Insertar";
        }

        //Metodo que permite desactivar un usuario en espefico, al seleccionarlo de la grilla.
        private async void btnEliminarUsuario_Click(object sender, EventArgs e)
        {
            string estadoUsuarioString;
            string estadoIdUsuario;

            if (dgvUsuarios.SelectedRows.Count > 0)
            {
                estadoIdUsuario = dgvUsuarios.CurrentRow.Cells["ID"].Value.ToString() + "/";
                estadoUsuarioString = dgvUsuarios.CurrentRow.Cells["ESTADO"].Value.ToString();

                if (estadoUsuarioString == "Deshabilitado")
                {
                    estadoIdUsuario = estadoIdUsuario + "1";
                }
                else
                {
                    estadoIdUsuario = estadoIdUsuario + "0";
                }

                if (await UsuarioController.cambiarEstadoUsuario(estadoIdUsuario))
                {
                    MessageBox.Show("Estado Actualizado");
                    ListarUsuariosAsync();

                }else
                {
                    MessageBox.Show("No se pudo completar la accion, intente nuevamente");
                }

            }
        }

        //Se establece que al escribir en el txtbox Confirmar el texto este oculto.
        private void txtConfirmar_TextChanged(object sender, EventArgs e)
        {
            txtConfirmar.UseSystemPasswordChar = true;
        }

        //Se establece que al escribir en el txtbox Contraseña el texto este oculto.
        private void txtContraseñaUsuario_TextChanged(object sender, EventArgs e)
        {
            txtContraseñaUsuario.UseSystemPasswordChar = true;
        }


        //Metodo que permite cambiar de nombre el boton Deshabilitar o Habilitar dependiendo del valor de la fila seleccionada.
        private void dgvUsuarios_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvUsuarios.SelectedRows.Count > 0)
            {
                if (dgvUsuarios.CurrentRow.Cells["ESTADO"].Value.ToString() == "Deshabilitado")
                {
                    btnEliminarUsuario.Text = "Habilitar";
                }
                else
                {
                    btnEliminarUsuario.Text = "Deshabilitar";
                }
            }
        }

        private void txtBuscador_KeyPress(object sender, KeyPressEventArgs e)
        {
            (dgvUsuarios.DataSource as DataTable).DefaultView.RowFilter = string.Format("NOMBRE LIKE'{0}%'", txtBuscador.Text);
        }

        private void btnCrearUsuario_Click(object sender, EventArgs e)
        {

            menuCrearEditar();
        }

        private void menuCrearEditar()
        {
            dgvUsuarios.Size = new Size(703, 381);
            btnEditarUsuario.Location = new Point(229, 521);
            btnEliminarUsuario.Location = new Point(456, 521);
            lblUsuarios.Location = new Point(337, 24);
            txtBuscador.Location = new Point(251, 67);
            lblBuscador.Location = new Point(189, 70);

            lblNombre.Visible = true;
            lblContraseña.Visible = true;
            lblConfimar.Visible = true;
            lblCorreo.Visible = true;
            lblCrearUsuario.Visible = true;
            lblPerfil.Visible = true;
            lblEstado.Visible = true;
            lblUsuario.Visible = true;


            txtCorreo.Visible = true;
            txtNombreUsuario.Visible = true;
            txtUsernameUsuario.Visible = true;
            txtConfirmar.Visible = true;
            txtContraseñaUsuario.Visible = true;
            cbEstadoUsuario.Visible = true;
            cbPerfilUsuario.Visible = true;

            btnCrearUsuario.Visible = false;
            btnGuardarUsuario.Visible = true;
            btnCancelar.Visible = true;

        }

        private void CancelarMenu()
        {
            dgvUsuarios.Size = new Size(1056, 381);
            btnEditarUsuario.Location = new Point(395, 521);
            btnEliminarUsuario.Location = new Point(637, 521);
            lblUsuarios.Location = new Point(542, 19);
            txtBuscador.Location = new Point(466, 64);
            lblBuscador.Location = new Point(404, 67);

            lblNombre.Visible = false;
            lblContraseña.Visible = false;
            lblConfimar.Visible = false;
            lblCorreo.Visible = false;
            lblCrearUsuario.Visible = false;
            lblPerfil.Visible = false;
            lblEstado.Visible = false;
            lblUsuario.Visible = false;


            txtCorreo.Visible = false;
            txtNombreUsuario.Visible = false;
            txtUsernameUsuario.Visible = false;
            txtConfirmar.Visible = false;
            txtContraseñaUsuario.Visible = false;
            cbEstadoUsuario.Visible = false;
            cbPerfilUsuario.Visible = false;

            btnCrearUsuario.Visible = true;
            btnGuardarUsuario.Visible = false;
            btnCancelar.Visible = false;
        }

            private void btnCancelar_Click(object sender, EventArgs e)
        {

            CancelarMenu();

        }
    }
}





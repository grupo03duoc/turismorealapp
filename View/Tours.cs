﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controller;
using Model;

namespace View
{
    public partial class Tours : Form
    {

        private bool editar = false;
        private int idTour;
        public Tours()
        {
            InitializeComponent();
        }

        private async void Tours_Load(object sender, EventArgs e)
        {
          await cargarRegionesAsync();
        }


        public async Task cargarRegionesAsync()
        {

            if (await RegionController.listarRegiones()!=null) {
                cbRegion.DisplayMember = "nombre";
                cbRegion.ValueMember = "id";
                cbRegion.DataSource = await RegionController.listarRegiones();
            }

        }

        private async void listarTours(int idComuna)
        {

            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("NOMBRE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("DESCRIPCION");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("VALOR PERSONA");
            dataTable.Columns.Add(dataColumn);

            if (await TourController.listarTours(idComuna)!=null) {
                foreach (Tour tours in await TourController.listarTours(idComuna))
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = tours.Id;
                    dataRow[1] = tours.Nombre;
                    dataRow[2] = tours.Descripcion;
                    dataRow[3] = tours.Valor_Persona;

                    dataTable.Rows.Add(dataRow);

                }

                dgvTours.DataSource = dataTable;
                dgvTours.Columns[0].Visible = false;
            }
        }


        private async void cbRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (await ProvinciaController.listarProvincias(int.Parse(cbRegion.SelectedValue.ToString()))!=null){
                cbProvincia.DisplayMember = "nombre";
                cbProvincia.ValueMember = "id";
                cbProvincia.DataSource = await ProvinciaController.listarProvincias(int.Parse(cbRegion.SelectedValue.ToString()));
            }
        }

        private async void cbProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (await ComunaController.listarComunas(int.Parse(cbProvincia.SelectedValue.ToString()))!=null){
                cbComuna.DisplayMember = "nombre";
                cbComuna.ValueMember = "id";
                cbComuna.DataSource = await ComunaController.listarComunas(int.Parse(cbProvincia.SelectedValue.ToString()));
            }
        }

        private void cbComuna_SelectedIndexChanged(object sender, EventArgs e)
        {
            listarTours(int.Parse(cbComuna.SelectedValue.ToString()));
        }

        private async void btnInsertarTour_Click(object sender, EventArgs e)
        {
            try
            {
 
                if (editar == false)
                {
                    if (await TourController.insertarTour(txtNombreTour.Text, txtDescripcion.Text, int.Parse(txtValorPersona.Text), int.Parse(cbRegion.SelectedValue.ToString()))) {
                        
                        MessageBox.Show("Tour Ingresado Correctamente");
                        listarTours(int.Parse(cbComuna.SelectedValue.ToString()));
                        limpiarFormTour();
                    }
                    else
                    {
                        MessageBox.Show("Tour No ingresado, intente nuevamente");
                        listarTours(int.Parse(cbComuna.SelectedValue.ToString()));
                     
                    }

                }
                else if (editar == true)
                {
                    if (await TourController.EditarTour(idTour, txtNombreTour.Text, txtDescripcion.Text, int.Parse(txtValorPersona.Text), int.Parse(cbRegion.SelectedValue.ToString())))
                    {
                        MessageBox.Show("Tour Editado Correctamente");
                        listarTours(int.Parse(cbComuna.SelectedValue.ToString()));
                        limpiarFormTour();
                    }
                    else
                    {
                        MessageBox.Show("Tour No Editado, intente nuevamente");
                        listarTours(int.Parse(cbComuna.SelectedValue.ToString()));
                    }    
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor verificar que ningun campo este vacio y con caracteres no correspondientes");

            }

        }

        private void btnCrearTour_Click(object sender, EventArgs e)
        {
            menuCrearEditar();
        }

        private void menuCrearEditar()
        {
            dgvTours.Size = new Size(712, 385);
            // btnEditarUsuario.Location = new Point(229, 521);
            // btnEliminarUsuario.Location = new Point(456, 521);
            lblRegion.Location = new Point(27, 66);
            cbRegion.Location = new Point(81, 65);
            lblProvincia.Location = new Point(275, 66);
            cbProvincia.Location = new Point(340, 65);
            lblComuna.Location = new Point(521, 66);
            cbComuna.Location = new Point(582, 65);
            lblBuscar.Location = new Point(242, 103);
            txtBuscar.Location = new Point(304, 100);
            lblTours.Location = new Point(353, 19);


            lblNombreTour.Visible = true;
            txtNombreTour.Visible = true;
            lblDescripcion.Visible = true;
            txtDescripcion.Visible = true;
            lblValorPersona.Visible = true;
            txtValorPersona.Visible = true;
            btnCancelar.Visible = true;
            btnCrearTour.Visible = false;
            btnInsertarTour.Visible = true;
            lblCrearTour.Visible = true;
            btnEditarTour.Visible = false;
        }



            private void btnCancelar_Click(object sender, EventArgs e)
        {
            cancelarMenu();
        }

        private void cancelarMenu()
        {
            dgvTours.Size = new Size(1054, 385);
            // btnEditarUsuario.Location = new Point(229, 521);
            // btnEliminarUsuario.Location = new Point(456, 521);
            lblRegion.Location = new Point(158, 69);
            cbRegion.Location = new Point(212, 68);
            lblProvincia.Location = new Point(406, 69);
            cbProvincia.Location = new Point(471, 68);
            lblComuna.Location = new Point(652, 69);
            cbComuna.Location = new Point(713, 68);
            lblBuscar.Location = new Point(373, 106);
            txtBuscar.Location = new Point(435, 103);
            lblTours.Location = new Point(511, 26);


            lblNombreTour.Visible = false;
            txtNombreTour.Visible = false;
            lblDescripcion.Visible = false;
            txtDescripcion.Visible = false;
            lblValorPersona.Visible = false;
            txtValorPersona.Visible = false;
            btnCancelar.Visible = false;
            btnCrearTour.Visible = true;
            btnInsertarTour.Visible = false;
            lblCrearTour.Visible = false;
            btnEditarTour.Visible = true;


        }

        private void btnEditarTour_Click(object sender, EventArgs e)
        {
            if (dgvTours.SelectedRows.Count > 0)
            {
                editar = true;
                menuCrearEditar();
                btnInsertarTour.Text = "Editar";
                idTour = int.Parse(dgvTours.CurrentRow.Cells["ID"].Value.ToString());

                txtNombreTour.Text = dgvTours.CurrentRow.Cells["NOMBRE"].Value.ToString();
                txtDescripcion.Text = dgvTours.CurrentRow.Cells["DESCRIPCION"].Value.ToString();
                txtValorPersona.Text = dgvTours.CurrentRow.Cells["VALOR PERSONA"].Value.ToString();

            }
            else
            {
                MessageBox.Show("Seleccione la fila a editar: ");
            }
        }


        private void limpiarFormTour()
        {
            txtNombreTour.Clear();
            txtDescripcion.Clear();
            txtValorPersona.Clear();

        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            (dgvTours.DataSource as DataTable).DefaultView.RowFilter = string.Format("NOMBRE LIKE'{0}%'", txtBuscar.Text);
        }
    }
}

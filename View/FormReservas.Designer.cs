﻿namespace View
{
    partial class FormReservas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label11 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.dtpFechaTermino = new System.Windows.Forms.DateTimePicker();
            this.dtpFechaInicio = new System.Windows.Forms.DateTimePicker();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPersonas = new System.Windows.Forms.TextBox();
            this.chkTraslado = new System.Windows.Forms.CheckBox();
            this.cbDepartamentos = new System.Windows.Forms.ComboBox();
            this.cbProvincia = new System.Windows.Forms.ComboBox();
            this.cbComuna = new System.Windows.Forms.ComboBox();
            this.cbRegion = new System.Windows.Forms.ComboBox();
            this.lblProvincia = new System.Windows.Forms.Label();
            this.lblComuna = new System.Windows.Forms.Label();
            this.lblRegion = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dpTrasladoFechaIni = new System.Windows.Forms.DateTimePicker();
            this.lblTrasIni = new System.Windows.Forms.Label();
            this.lblSeleccionarTraslado = new System.Windows.Forms.Label();
            this.lblDirRetiro = new System.Windows.Forms.Label();
            this.txtDirRetiro = new System.Windows.Forms.TextBox();
            this.cbEmpresa = new System.Windows.Forms.ComboBox();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.cbChoferes = new System.Windows.Forms.ComboBox();
            this.lblChoferes = new System.Windows.Forms.Label();
            this.chbConfirmarReserva = new System.Windows.Forms.CheckBox();
            this.btnRealizarReserva = new System.Windows.Forms.Button();
            this.lblBuscadorDos = new System.Windows.Forms.Label();
            this.txtBuscador = new System.Windows.Forms.TextBox();
            this.dgvUsuarios = new System.Windows.Forms.DataGridView();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.dgvTours = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.chbTours = new System.Windows.Forms.CheckBox();
            this.lblSeleccionarTour = new System.Windows.Forms.Label();
            this.btnTotal = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTours)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label11.Location = new System.Drawing.Point(545, 32);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(168, 29);
            this.label11.TabIndex = 93;
            this.label11.Text = "Crear Reserva";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTotal.Location = new System.Drawing.Point(201, 555);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(66, 25);
            this.lblTotal.TabIndex = 87;
            this.lblTotal.Text = "Total:";
            // 
            // dtpFechaTermino
            // 
            this.dtpFechaTermino.Location = new System.Drawing.Point(279, 414);
            this.dtpFechaTermino.Name = "dtpFechaTermino";
            this.dtpFechaTermino.Size = new System.Drawing.Size(244, 20);
            this.dtpFechaTermino.TabIndex = 83;
            // 
            // dtpFechaInicio
            // 
            this.dtpFechaInicio.Location = new System.Drawing.Point(279, 388);
            this.dtpFechaInicio.Name = "dtpFechaInicio";
            this.dtpFechaInicio.Size = new System.Drawing.Size(244, 20);
            this.dtpFechaInicio.TabIndex = 82;
            // 
            // txtTotal
            // 
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.Location = new System.Drawing.Point(279, 557);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(244, 23);
            this.txtTotal.TabIndex = 81;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(166, 443);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 79;
            this.label4.Text = "Personas:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(166, 418);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 17);
            this.label3.TabIndex = 78;
            this.label3.Text = "Fecha Termino:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(166, 392);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 17);
            this.label2.TabIndex = 77;
            this.label2.Text = "Fecha Inicio:";
            // 
            // txtPersonas
            // 
            this.txtPersonas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPersonas.Location = new System.Drawing.Point(279, 440);
            this.txtPersonas.Name = "txtPersonas";
            this.txtPersonas.Size = new System.Drawing.Size(244, 23);
            this.txtPersonas.TabIndex = 76;
            this.txtPersonas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPersonas_KeyPress);
            // 
            // chkTraslado
            // 
            this.chkTraslado.AutoSize = true;
            this.chkTraslado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTraslado.Location = new System.Drawing.Point(439, 484);
            this.chkTraslado.Name = "chkTraslado";
            this.chkTraslado.Size = new System.Drawing.Size(82, 20);
            this.chkTraslado.TabIndex = 94;
            this.chkTraslado.Text = "Traslado";
            this.chkTraslado.UseVisualStyleBackColor = true;
            this.chkTraslado.Click += new System.EventHandler(this.chkTraslado_Click);
            // 
            // cbDepartamentos
            // 
            this.cbDepartamentos.FormattingEnabled = true;
            this.cbDepartamentos.Location = new System.Drawing.Point(279, 336);
            this.cbDepartamentos.Name = "cbDepartamentos";
            this.cbDepartamentos.Size = new System.Drawing.Size(242, 21);
            this.cbDepartamentos.TabIndex = 124;
            // 
            // cbProvincia
            // 
            this.cbProvincia.FormattingEnabled = true;
            this.cbProvincia.Location = new System.Drawing.Point(281, 251);
            this.cbProvincia.Name = "cbProvincia";
            this.cbProvincia.Size = new System.Drawing.Size(242, 21);
            this.cbProvincia.TabIndex = 122;
            this.cbProvincia.SelectedIndexChanged += new System.EventHandler(this.cbProvincia_SelectedIndexChanged);
            // 
            // cbComuna
            // 
            this.cbComuna.FormattingEnabled = true;
            this.cbComuna.Location = new System.Drawing.Point(281, 279);
            this.cbComuna.Name = "cbComuna";
            this.cbComuna.Size = new System.Drawing.Size(242, 21);
            this.cbComuna.TabIndex = 121;
            this.cbComuna.SelectedIndexChanged += new System.EventHandler(this.cbComuna_SelectedIndexChanged);
            // 
            // cbRegion
            // 
            this.cbRegion.FormattingEnabled = true;
            this.cbRegion.Location = new System.Drawing.Point(281, 218);
            this.cbRegion.Name = "cbRegion";
            this.cbRegion.Size = new System.Drawing.Size(242, 21);
            this.cbRegion.TabIndex = 120;
            this.cbRegion.SelectedIndexChanged += new System.EventHandler(this.cbRegion_SelectedIndexChanged);
            // 
            // lblProvincia
            // 
            this.lblProvincia.AutoSize = true;
            this.lblProvincia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProvincia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblProvincia.Location = new System.Drawing.Point(168, 249);
            this.lblProvincia.Name = "lblProvincia";
            this.lblProvincia.Size = new System.Drawing.Size(70, 17);
            this.lblProvincia.TabIndex = 119;
            this.lblProvincia.Text = "Provincia:";
            // 
            // lblComuna
            // 
            this.lblComuna.AutoSize = true;
            this.lblComuna.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComuna.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblComuna.Location = new System.Drawing.Point(168, 277);
            this.lblComuna.Name = "lblComuna";
            this.lblComuna.Size = new System.Drawing.Size(64, 17);
            this.lblComuna.TabIndex = 118;
            this.lblComuna.Text = "Comuna:";
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblRegion.Location = new System.Drawing.Point(168, 216);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(57, 17);
            this.lblRegion.TabIndex = 117;
            this.lblRegion.Text = "Region:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(323, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 20);
            this.label1.TabIndex = 125;
            this.label1.Text = "Seleccionar Usuario";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(294, 313);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(199, 20);
            this.label5.TabIndex = 126;
            this.label5.Text = "Seleccionar Departamento";
            // 
            // dpTrasladoFechaIni
            // 
            this.dpTrasladoFechaIni.Location = new System.Drawing.Point(700, 129);
            this.dpTrasladoFechaIni.Name = "dpTrasladoFechaIni";
            this.dpTrasladoFechaIni.Size = new System.Drawing.Size(244, 20);
            this.dpTrasladoFechaIni.TabIndex = 129;
            this.dpTrasladoFechaIni.Visible = false;
            // 
            // lblTrasIni
            // 
            this.lblTrasIni.AutoSize = true;
            this.lblTrasIni.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrasIni.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTrasIni.Location = new System.Drawing.Point(587, 133);
            this.lblTrasIni.Name = "lblTrasIni";
            this.lblTrasIni.Size = new System.Drawing.Size(55, 17);
            this.lblTrasIni.TabIndex = 127;
            this.lblTrasIni.Text = "Fecha :";
            this.lblTrasIni.Visible = false;
            // 
            // lblSeleccionarTraslado
            // 
            this.lblSeleccionarTraslado.AutoSize = true;
            this.lblSeleccionarTraslado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeleccionarTraslado.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSeleccionarTraslado.Location = new System.Drawing.Point(747, 97);
            this.lblSeleccionarTraslado.Name = "lblSeleccionarTraslado";
            this.lblSeleccionarTraslado.Size = new System.Drawing.Size(153, 20);
            this.lblSeleccionarTraslado.TabIndex = 131;
            this.lblSeleccionarTraslado.Text = "Seleccionar traslado";
            this.lblSeleccionarTraslado.Visible = false;
            // 
            // lblDirRetiro
            // 
            this.lblDirRetiro.AutoSize = true;
            this.lblDirRetiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDirRetiro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDirRetiro.Location = new System.Drawing.Point(587, 155);
            this.lblDirRetiro.Name = "lblDirRetiro";
            this.lblDirRetiro.Size = new System.Drawing.Size(91, 17);
            this.lblDirRetiro.TabIndex = 133;
            this.lblDirRetiro.Text = "Direc. Retiro:";
            this.lblDirRetiro.Visible = false;
            // 
            // txtDirRetiro
            // 
            this.txtDirRetiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDirRetiro.Location = new System.Drawing.Point(700, 155);
            this.txtDirRetiro.Name = "txtDirRetiro";
            this.txtDirRetiro.Size = new System.Drawing.Size(244, 23);
            this.txtDirRetiro.TabIndex = 132;
            this.txtDirRetiro.Visible = false;
            // 
            // cbEmpresa
            // 
            this.cbEmpresa.FormattingEnabled = true;
            this.cbEmpresa.Location = new System.Drawing.Point(700, 184);
            this.cbEmpresa.Name = "cbEmpresa";
            this.cbEmpresa.Size = new System.Drawing.Size(244, 21);
            this.cbEmpresa.TabIndex = 135;
            this.cbEmpresa.Visible = false;
            this.cbEmpresa.SelectedIndexChanged += new System.EventHandler(this.cbEmpresa_SelectedIndexChanged);
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.AutoSize = true;
            this.lblEmpresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpresa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblEmpresa.Location = new System.Drawing.Point(587, 185);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(68, 17);
            this.lblEmpresa.TabIndex = 134;
            this.lblEmpresa.Text = "Empresa:";
            this.lblEmpresa.Visible = false;
            // 
            // cbChoferes
            // 
            this.cbChoferes.FormattingEnabled = true;
            this.cbChoferes.Location = new System.Drawing.Point(700, 211);
            this.cbChoferes.Name = "cbChoferes";
            this.cbChoferes.Size = new System.Drawing.Size(244, 21);
            this.cbChoferes.TabIndex = 137;
            this.cbChoferes.Visible = false;
            // 
            // lblChoferes
            // 
            this.lblChoferes.AutoSize = true;
            this.lblChoferes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChoferes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblChoferes.Location = new System.Drawing.Point(587, 212);
            this.lblChoferes.Name = "lblChoferes";
            this.lblChoferes.Size = new System.Drawing.Size(69, 17);
            this.lblChoferes.TabIndex = 136;
            this.lblChoferes.Text = "Choferes:";
            this.lblChoferes.Visible = false;
            // 
            // chbConfirmarReserva
            // 
            this.chbConfirmarReserva.AutoSize = true;
            this.chbConfirmarReserva.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbConfirmarReserva.Location = new System.Drawing.Point(763, 418);
            this.chbConfirmarReserva.Name = "chbConfirmarReserva";
            this.chbConfirmarReserva.Size = new System.Drawing.Size(84, 20);
            this.chbConfirmarReserva.TabIndex = 138;
            this.chbConfirmarReserva.Text = "Confirmar";
            this.chbConfirmarReserva.UseVisualStyleBackColor = true;
            this.chbConfirmarReserva.Visible = false;
            // 
            // btnRealizarReserva
            // 
            this.btnRealizarReserva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnRealizarReserva.FlatAppearance.BorderSize = 0;
            this.btnRealizarReserva.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRealizarReserva.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRealizarReserva.ForeColor = System.Drawing.Color.White;
            this.btnRealizarReserva.Location = new System.Drawing.Point(751, 444);
            this.btnRealizarReserva.Name = "btnRealizarReserva";
            this.btnRealizarReserva.Size = new System.Drawing.Size(124, 35);
            this.btnRealizarReserva.TabIndex = 139;
            this.btnRealizarReserva.Text = "Realizar Reserva";
            this.btnRealizarReserva.UseVisualStyleBackColor = false;
            this.btnRealizarReserva.Click += new System.EventHandler(this.btnRealizarReserva_Click);
            // 
            // lblBuscadorDos
            // 
            this.lblBuscadorDos.AutoSize = true;
            this.lblBuscadorDos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscadorDos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblBuscadorDos.Location = new System.Drawing.Point(166, 77);
            this.lblBuscadorDos.Name = "lblBuscadorDos";
            this.lblBuscadorDos.Size = new System.Drawing.Size(56, 17);
            this.lblBuscadorDos.TabIndex = 142;
            this.lblBuscadorDos.Text = "Buscar:";
            // 
            // txtBuscador
            // 
            this.txtBuscador.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscador.Location = new System.Drawing.Point(279, 77);
            this.txtBuscador.Name = "txtBuscador";
            this.txtBuscador.Size = new System.Drawing.Size(242, 23);
            this.txtBuscador.TabIndex = 141;
            this.txtBuscador.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBuscador_KeyPress);
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.AllowUserToAddRows = false;
            this.dgvUsuarios.AllowUserToDeleteRows = false;
            this.dgvUsuarios.AllowUserToOrderColumns = true;
            this.dgvUsuarios.AllowUserToResizeColumns = false;
            this.dgvUsuarios.AllowUserToResizeRows = false;
            this.dgvUsuarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsuarios.Location = new System.Drawing.Point(125, 106);
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.ReadOnly = true;
            this.dgvUsuarios.Size = new System.Drawing.Size(398, 96);
            this.dgvUsuarios.TabIndex = 140;
            // 
            // txtBuscar
            // 
            this.txtBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscar.Location = new System.Drawing.Point(700, 271);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(244, 23);
            this.txtBuscar.TabIndex = 145;
            this.txtBuscar.Visible = false;
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblBuscar.Location = new System.Drawing.Point(587, 274);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(56, 17);
            this.lblBuscar.TabIndex = 144;
            this.lblBuscar.Text = "Buscar:";
            this.lblBuscar.Visible = false;
            // 
            // dgvTours
            // 
            this.dgvTours.AllowUserToAddRows = false;
            this.dgvTours.AllowUserToDeleteRows = false;
            this.dgvTours.AllowUserToOrderColumns = true;
            this.dgvTours.AllowUserToResizeColumns = false;
            this.dgvTours.AllowUserToResizeRows = false;
            this.dgvTours.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTours.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTours.Location = new System.Drawing.Point(590, 310);
            this.dgvTours.Name = "dgvTours";
            this.dgvTours.ReadOnly = true;
            this.dgvTours.Size = new System.Drawing.Size(354, 82);
            this.dgvTours.TabIndex = 143;
            this.dgvTours.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(168, 340);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 17);
            this.label7.TabIndex = 146;
            this.label7.Text = "Departamento:";
            // 
            // chbTours
            // 
            this.chbTours.AutoSize = true;
            this.chbTours.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chbTours.Location = new System.Drawing.Point(281, 484);
            this.chbTours.Name = "chbTours";
            this.chbTours.Size = new System.Drawing.Size(62, 20);
            this.chbTours.TabIndex = 147;
            this.chbTours.Text = "Tours";
            this.chbTours.UseVisualStyleBackColor = true;
            this.chbTours.Click += new System.EventHandler(this.chbTours_Click);
            // 
            // lblSeleccionarTour
            // 
            this.lblSeleccionarTour.AutoSize = true;
            this.lblSeleccionarTour.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeleccionarTour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSeleccionarTour.Location = new System.Drawing.Point(759, 246);
            this.lblSeleccionarTour.Name = "lblSeleccionarTour";
            this.lblSeleccionarTour.Size = new System.Drawing.Size(128, 20);
            this.lblSeleccionarTour.TabIndex = 148;
            this.lblSeleccionarTour.Text = "Seleccionar Tour";
            this.lblSeleccionarTour.Visible = false;
            // 
            // btnTotal
            // 
            this.btnTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(61)))), ((int)(((byte)(92)))));
            this.btnTotal.FlatAppearance.BorderSize = 0;
            this.btnTotal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotal.ForeColor = System.Drawing.Color.White;
            this.btnTotal.Location = new System.Drawing.Point(67, 557);
            this.btnTotal.Name = "btnTotal";
            this.btnTotal.Size = new System.Drawing.Size(95, 28);
            this.btnTotal.TabIndex = 149;
            this.btnTotal.Text = "Calcular Total";
            this.btnTotal.UseVisualStyleBackColor = false;
            this.btnTotal.Click += new System.EventHandler(this.btnTotal_Click);
            // 
            // FormReservas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1138, 606);
            this.Controls.Add(this.btnTotal);
            this.Controls.Add(this.lblSeleccionarTour);
            this.Controls.Add(this.chbTours);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.lblBuscar);
            this.Controls.Add(this.dgvTours);
            this.Controls.Add(this.lblBuscadorDos);
            this.Controls.Add(this.txtBuscador);
            this.Controls.Add(this.dgvUsuarios);
            this.Controls.Add(this.btnRealizarReserva);
            this.Controls.Add(this.chbConfirmarReserva);
            this.Controls.Add(this.cbChoferes);
            this.Controls.Add(this.lblChoferes);
            this.Controls.Add(this.cbEmpresa);
            this.Controls.Add(this.lblEmpresa);
            this.Controls.Add(this.lblDirRetiro);
            this.Controls.Add(this.txtDirRetiro);
            this.Controls.Add(this.lblSeleccionarTraslado);
            this.Controls.Add(this.dpTrasladoFechaIni);
            this.Controls.Add(this.lblTrasIni);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbDepartamentos);
            this.Controls.Add(this.cbProvincia);
            this.Controls.Add(this.cbComuna);
            this.Controls.Add(this.cbRegion);
            this.Controls.Add(this.lblProvincia);
            this.Controls.Add(this.lblComuna);
            this.Controls.Add(this.lblRegion);
            this.Controls.Add(this.chkTraslado);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.dtpFechaTermino);
            this.Controls.Add(this.dtpFechaInicio);
            this.Controls.Add(this.txtTotal);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPersonas);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormReservas";
            this.Text = "FormReservas";
            this.Load += new System.EventHandler(this.FormReservas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.DateTimePicker dtpFechaTermino;
        private System.Windows.Forms.DateTimePicker dtpFechaInicio;
        public System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtPersonas;
        private System.Windows.Forms.CheckBox chkTraslado;
        private System.Windows.Forms.ComboBox cbDepartamentos;
        private System.Windows.Forms.ComboBox cbProvincia;
        private System.Windows.Forms.ComboBox cbComuna;
        private System.Windows.Forms.ComboBox cbRegion;
        private System.Windows.Forms.Label lblProvincia;
        private System.Windows.Forms.Label lblComuna;
        private System.Windows.Forms.Label lblRegion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dpTrasladoFechaIni;
        private System.Windows.Forms.Label lblTrasIni;
        private System.Windows.Forms.Label lblSeleccionarTraslado;
        private System.Windows.Forms.Label lblDirRetiro;
        public System.Windows.Forms.TextBox txtDirRetiro;
        private System.Windows.Forms.ComboBox cbEmpresa;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.ComboBox cbChoferes;
        private System.Windows.Forms.Label lblChoferes;
        private System.Windows.Forms.CheckBox chbConfirmarReserva;
        private System.Windows.Forms.Button btnRealizarReserva;
        private System.Windows.Forms.Label lblBuscadorDos;
        public System.Windows.Forms.TextBox txtBuscador;
        private System.Windows.Forms.DataGridView dgvUsuarios;
        public System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.DataGridView dgvTours;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chbTours;
        private System.Windows.Forms.Label lblSeleccionarTour;
        private System.Windows.Forms.Button btnTotal;
    }
}
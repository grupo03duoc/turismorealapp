﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controller;
using System.Windows;
using System.Threading.Tasks;
using System.ComponentModel;

namespace View
{
    public partial class Reportes : Form
    {
        public Reportes()
        {
            InitializeComponent();
        }

        int idRegion;
        public async Task cargarRegionesAsync()
        {
            if (await RegionController.listarRegiones() != null)
            {
                cbRegion.DisplayMember = "nombre";
                cbRegion.ValueMember = "id";
                cbRegion.DataSource = await RegionController.listarRegiones();
            }

        }

        private async void btnGenerarReporte_Click(object sender, EventArgs e)
        {
            string fechaInicio = dtpFechaInicio.Value.ToString("dd-MM-yyyy");
            string fechaTermino = dtpFechaTermino.Value.ToString("dd-MM-yyyy");
            int idRegion = int.Parse(cbRegion.SelectedValue.ToString());

            lblDescargando.Visible = true;

            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Reporte_TurismoReal"; 
            dlg.DefaultExt = ".xlsx";
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                WebClient webClient = new WebClient();
                webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                webClient.DownloadFileAsync(new Uri("http://turismoreal.us-east-1.elasticbeanstalk.com/reporte/generar/" + fechaInicio + "/" + fechaTermino + "/" + idRegion),
                    dlg.FileName);
                string filename = dlg.FileName;

            }
        }

        private void cbRegion_Click(object sender, EventArgs e)
        {

            idRegion = int.Parse(cbRegion.SelectedValue.ToString());
        }

        private async void Reportes_Load(object sender, EventArgs e)
        {
            await cargarRegionesAsync();
        }

        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            MessageBox.Show("Descarga Completa!");
            lblDescargando.Visible = false;
        }

    }
}

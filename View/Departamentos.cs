﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controller;
using Model;

namespace View
{
    public partial class Departamentos : Form
    {
        private int idDepartamento;
        private bool editar = false;
        private int idComuna;
        List<Equipamiento> equipamientos = new List<Equipamiento>();
        List<Foto> fotos = new List<Foto>();

        public Departamentos()
        {
            InitializeComponent();

        }

        public async Task cargarRegionesAsync()
        {
            if (await RegionController.listarRegiones()!=null) {
                cbRegion.DisplayMember = "nombre";
                cbRegion.ValueMember = "id";
                cbRegion.DataSource = await RegionController.listarRegiones();
            }

        }

        private async void Departamento_Load(object sender, EventArgs e)
        {

           await cargarRegionesAsync();
        }

        private async void cbRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (await ProvinciaController.listarProvincias(int.Parse(cbRegion.SelectedValue.ToString()))!=null) {
                cbProvincia.DisplayMember = "nombre";
                cbProvincia.ValueMember = "id";
                cbProvincia.DataSource = await ProvinciaController.listarProvincias(int.Parse(cbRegion.SelectedValue.ToString()));
            }

        }

        private async void cbProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (await ComunaController.listarComunas(int.Parse(cbProvincia.SelectedValue.ToString()))!=null)
            {
                cbComuna.DisplayMember = "nombre";
                cbComuna.ValueMember = "id";
                cbComuna.DataSource = await ComunaController.listarComunas(int.Parse(cbProvincia.SelectedValue.ToString()));
            }
        }

        private void cbComuna_SelectedIndexChanged(object sender, EventArgs e)
        {

            listarDepartamentos(int.Parse(cbComuna.SelectedValue.ToString()));
        }

        private async void listarDepartamentos(int idComuna)
        {
            DataTable dataTableIventario = new DataTable();
            DataColumn dataColumnInventario;
            dataColumnInventario = new DataColumn("ID");
            dataTableIventario.Columns.Add(dataColumnInventario);
            dataColumnInventario = new DataColumn("NOMBRE");
            dataTableIventario.Columns.Add(dataColumnInventario);
            dataColumnInventario = new DataColumn("CANTIDAD");
            dataTableIventario.Columns.Add(dataColumnInventario);
            dataColumnInventario = new DataColumn("VALOR UNITARIO");
            dataTableIventario.Columns.Add(dataColumnInventario);

            DataTable dataTable = new DataTable();
            DataColumn dataColumn;
            dataColumn = new DataColumn("ID");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("DIRECCION");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("CARACTERISTICAS");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("VALOR NOCHE");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("DORMITORIOS");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("CAMAS");
            dataTable.Columns.Add(dataColumn);
            dataColumn = new DataColumn("COMUNA");
            dataTable.Columns.Add(dataColumn);

            if (await DepartamentoController.listarDepartamentos(idComuna)!=null) {
                foreach (Departamento departamento in await DepartamentoController.listarDepartamentos(idComuna))
                {
                    DataRow dataRow = dataTable.NewRow();
                    dataRow[0] = departamento.Id;
                    dataRow[1] = departamento.Direccion;
                    dataRow[2] = departamento.Caracteristicas;
                    dataRow[3] = departamento.ValorNoche;
                    dataRow[4] = departamento.Dormitorios;
                    dataRow[5] = departamento.Camas;
                    dataRow[6] = departamento.Comuna.Nombre;
                    dataTable.Rows.Add(dataRow);

                }

                dgvDepartamentos.DataSource = dataTable;
                dgvDepartamentos.Columns[0].Visible = false;
            }

        }

        private async void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {

                if (editar == false)
                {
                    if (await DepartamentoController.insertarDepartamento(txtDireccion.Text, txtCaracteristicas.Text, int.Parse(txtValorNoche.Text), int.Parse(txtDormitorios.Text), int.Parse(txtCamas.Text), int.Parse(cbComuna.SelectedValue.ToString()))){
                        
                        MessageBox.Show("Departamento Ingresado Correctamente");
                        listarDepartamentos(int.Parse(cbComuna.SelectedValue.ToString()));
                        limpiarFormDepartamento();

                    }
                    else
                    {
                        MessageBox.Show("Departamento No ingresado, intente nuevamente");
                        listarDepartamentos(int.Parse(cbComuna.SelectedValue.ToString()));
                     
                    }
                }
                else if (editar == true)
                {
                    if (await DepartamentoController.EditarDepartamento(idDepartamento, txtDireccion.Text, txtCaracteristicas.Text, int.Parse(txtValorNoche.Text), int.Parse(txtDormitorios.Text), int.Parse(txtCamas.Text), 1, 0));
                    {

                        MessageBox.Show("Departamento Editado Correctamente");
                        listarDepartamentos(int.Parse(cbComuna.SelectedValue.ToString()));
                        limpiarFormDepartamento();
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor verificar que ningun campo este vacio y con caracteres no correspondientes");

            }
        }



        //Metodo que permite limpiar todos los combobox y textbox.
        private void limpiarFormDepartamento()
        {
            txtDireccion.Clear();
            txtCaracteristicas.Clear();
            txtValorNoche.Text = "";
            txtDormitorios.Clear();
            txtCamas.Text = "";    
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dgvDepartamentos.SelectedRows.Count > 0)
            {
                limpiarFormDepartamento();
                lblCrearDepartamento.Text = "Editar Departamento";
                idDepartamento = int.Parse(dgvDepartamentos.CurrentRow.Cells["ID"].Value.ToString());
                editar = true;
                txtDireccion.Text = dgvDepartamentos.CurrentRow.Cells["DIRECCION"].Value.ToString();
                txtCaracteristicas.Text = dgvDepartamentos.CurrentRow.Cells["CARACTERISTICAS"].Value.ToString();
                txtValorNoche.Text = dgvDepartamentos.CurrentRow.Cells["VALOR NOCHE"].Value.ToString();
                txtDormitorios.Text = dgvDepartamentos.CurrentRow.Cells["DORMITORIOS"].Value.ToString();
                txtCamas.Text = dgvDepartamentos.CurrentRow.Cells["CAMAS"].Value.ToString();

            }
            else
            {
                MessageBox.Show("Seleccione la fila a editar: ");
            }
        }

        private void menuCrearEditar()
        {

            dgvDepartamentos.Size = new Size(703, 445);
            lblDepartamentos.Location = new Point(301, 19);
            lblRegion.Location = new Point(23, 65);
            cbRegion.Location = new Point(75, 64);
            lblComuna.Location = new Point(515, 65);
            cbComuna.Location = new Point(576, 64);
            lblProvincia.Location = new Point(269, 65);
            cbProvincia.Location = new Point(334, 64);

            lblCaracteristicas.Visible = true;
            lblCamas.Visible = true;
            lblValorNoche.Visible = true;
            lblDireccion.Visible = true;
            lblDormitorios.Visible = true;
            lblCrearDepartamento.Visible = true;

            txtCaracteristicas.Visible = true;
            txtCamas.Visible = true;
            txtDireccion.Visible = true;
            txtValorNoche.Visible = true;
            txtDormitorios.Visible = true;


            btnAgregarDepartamentos.Visible = false;
            btnAgregar.Visible = true;
            btnCancelar.Visible = true;
        }

        private void btnAgregarDepartamentos_Click(object sender, EventArgs e)
        {
            menuCrearEditar();
        }

        private void CancelarMenu()
        {
            dgvDepartamentos.Size = new Size(1059, 445);
            lblDepartamentos.Location = new Point(474, 20);
            lblRegion.Location = new Point(196, 66);
            cbRegion.Location = new Point(248, 65);
            lblComuna.Location = new Point(688, 66);
            cbComuna.Location = new Point(749, 65);
            lblProvincia.Location = new Point(442, 66);
            cbProvincia.Location = new Point(507, 65);
            lblCrearDepartamento.Text = "Crear Departamento";

            lblCaracteristicas.Visible = false;
            lblCamas.Visible = false;
            lblValorNoche.Visible = false;
            lblDireccion.Visible = false;
            lblDormitorios.Visible = false;
            lblCrearDepartamento.Visible = false;

            txtCaracteristicas.Visible = false;
            txtCamas.Visible = false;
            txtDireccion.Visible = false;
            txtValorNoche.Visible = false;
            txtDormitorios.Visible = false;


            btnAgregarDepartamentos.Visible = true;
            btnAgregar.Visible = false;
            btnCancelar.Visible = false;

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {

            CancelarMenu();
        }

        private void btnEditarDepartamento_Click(object sender, EventArgs e)
        {
            if (dgvDepartamentos.SelectedRows.Count > 0)
            {
                menuCrearEditar();
                idDepartamento = int.Parse(dgvDepartamentos.CurrentRow.Cells["ID"].Value.ToString());
                btnAgregar.Text ="Editar";
                editar = true;
                txtDireccion.Text = dgvDepartamentos.CurrentRow.Cells["DIRECCION"].Value.ToString();
                txtCaracteristicas.Text = dgvDepartamentos.CurrentRow.Cells["CARACTERISTICAS"].Value.ToString();
                txtValorNoche.Text = dgvDepartamentos.CurrentRow.Cells["VALOR NOCHE"].Value.ToString();
                txtDormitorios.Text = dgvDepartamentos.CurrentRow.Cells["DORMITORIOS"].Value.ToString();
                txtCamas.Text = dgvDepartamentos.CurrentRow.Cells["CAMAS"].Value.ToString();
                txtCamas.Text = dgvDepartamentos.CurrentRow.Cells["CAMAS"].Value.ToString();
            }
            else
            {
                MessageBox.Show("Seleccione la fila a editar: ");
            }
        }
    }

}

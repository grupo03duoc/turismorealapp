﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Controller;


namespace View
{
    public partial class Login : Form 
    {

        MenuInicio menu = new MenuInicio();
        public Login()
        {
            InitializeComponent();
        }

        //Se agregan DLL para configurar form y asi poder arrastrar la ventana de un lugar a otro
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wwmsg, int wparam, int lparam);


        //Se configura si se muestra usuario o correo se vacie el combobox
        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "Usuario o Correo")
            {
                txtUsuario.Text = "";
                txtUsuario.ForeColor = Color.LightGray;
            }
        }

        //Se configura si usuario o correo esta vacia este vuelva a tener el valor usuario o correo 
        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            if (txtUsuario.Text == "")
            {
                txtUsuario.Text = "Usuario o Correo";
                txtUsuario.ForeColor = Color.DimGray;
            } 
        }

        //Se configura si se muestra Contraseña se vacie el combobox y se agrega la opcion de ocultar caracteres 
        private void txtContraseña_Enter(object sender, EventArgs e)
        {
            if (txtContraseña.Text == "Contraseña")
            {
                txtContraseña.Text = "";
                txtContraseña.ForeColor = Color.LightGray;
                txtContraseña.UseSystemPasswordChar = true;
            }
        }

        //Se configura si el combobox contraseña esta vacia este vuelva a tener el valor Contraseña
        private void txtContraseña_Leave(object sender, EventArgs e)
        {
            if (txtContraseña.Text == "")
            {
                txtContraseña.Text = "Contraseña";
                txtContraseña.ForeColor = Color.DimGray;
                txtContraseña.UseSystemPasswordChar = false;

            }
        }

        //Se configura el boton minimizar para minimizar la aplicacion una vez se presione el boton.
        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        //Se configura el boton cerrar para cerrar la aplicacion una vez se presione el boton.
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de cerrar la aplicacion?", "Alerta", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        //Se configura el arrastrar ventana de un lugar a otro
        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private async void btnIngresar_Click(object sender, EventArgs e)
        {

            if (await UsuarioController.LoginUsuario(txtUsuario.Text, txtContraseña.Text))
            {
                MenuInicio menu = new MenuInicio();
                menu.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Usuario o contraseña incorrecta");
            }
        }
    }
}
